# Python API client for PeerTube

This Python package is automatically generated from [PeerTube's REST API](https://docs.joinpeertube.org/api-rest-reference.html),
using the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 2.4.0
- Package version: 1.0.0
- Build package: org.openapitools.codegen.languages.PythonClientCodegen

For more information, please visit [https://joinpeertube.org](https://joinpeertube.org)

## Requirements.

Python 2.7 and 3.4+

## Installation & Usage

```sh
pip install git+https://framagit.org/framasoft/peertube/clients/python.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://framagit.org/framasoft/peertube/clients/python.git`)

Then import the package:
```python
import peertube
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```python
from __future__ import print_function

import time
import peertube
from peertube.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'


# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.AbusesApi(api_client)
    abuse_id = 56 # int | Abuse id

    try:
        # Delete an abuse
        api_instance.abuses_abuse_id_delete(abuse_id)
    except ApiException as e:
        print("Exception when calling AbusesApi->abuses_abuse_id_delete: %s\n" % e)
    
```

## Documentation for API Endpoints

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AbusesApi* | [**abuses_abuse_id_delete**](docs/AbusesApi.md#abuses_abuse_id_delete) | **DELETE** /abuses/{abuseId} | Delete an abuse
*AbusesApi* | [**abuses_abuse_id_messages_abuse_message_id_delete**](docs/AbusesApi.md#abuses_abuse_id_messages_abuse_message_id_delete) | **DELETE** /abuses/{abuseId}/messages/{abuseMessageId} | Delete an abuse message
*AbusesApi* | [**abuses_abuse_id_messages_get**](docs/AbusesApi.md#abuses_abuse_id_messages_get) | **GET** /abuses/{abuseId}/messages | List messages of an abuse
*AbusesApi* | [**abuses_abuse_id_messages_post**](docs/AbusesApi.md#abuses_abuse_id_messages_post) | **POST** /abuses/{abuseId}/messages | Add message to an abuse
*AbusesApi* | [**abuses_abuse_id_put**](docs/AbusesApi.md#abuses_abuse_id_put) | **PUT** /abuses/{abuseId} | Update an abuse
*AbusesApi* | [**abuses_get**](docs/AbusesApi.md#abuses_get) | **GET** /abuses | List abuses
*AbusesApi* | [**abuses_post**](docs/AbusesApi.md#abuses_post) | **POST** /abuses | Report an abuse
*AbusesApi* | [**users_me_abuses_get**](docs/AbusesApi.md#users_me_abuses_get) | **GET** /users/me/abuses | List my abuses
*AccountBlocksApi* | [**blocklist_accounts_account_name_delete**](docs/AccountBlocksApi.md#blocklist_accounts_account_name_delete) | **DELETE** /blocklist/accounts/{accountName} | Unblock an account by its handle
*AccountBlocksApi* | [**blocklist_accounts_get**](docs/AccountBlocksApi.md#blocklist_accounts_get) | **GET** /blocklist/accounts | List account blocks
*AccountBlocksApi* | [**blocklist_accounts_post**](docs/AccountBlocksApi.md#blocklist_accounts_post) | **POST** /blocklist/accounts | Block an account
*AccountsApi* | [**accounts_get**](docs/AccountsApi.md#accounts_get) | **GET** /accounts | List accounts
*AccountsApi* | [**accounts_name_get**](docs/AccountsApi.md#accounts_name_get) | **GET** /accounts/{name} | Get an account
*AccountsApi* | [**accounts_name_ratings_get**](docs/AccountsApi.md#accounts_name_ratings_get) | **GET** /accounts/{name}/ratings | List ratings of an account
*AccountsApi* | [**accounts_name_video_channels_get**](docs/AccountsApi.md#accounts_name_video_channels_get) | **GET** /accounts/{name}/video-channels | List video channels of an account
*AccountsApi* | [**accounts_name_videos_get**](docs/AccountsApi.md#accounts_name_videos_get) | **GET** /accounts/{name}/videos | List videos of an account
*ConfigApi* | [**config_about_get**](docs/ConfigApi.md#config_about_get) | **GET** /config/about | Get instance \&quot;About\&quot; information
*ConfigApi* | [**config_custom_delete**](docs/ConfigApi.md#config_custom_delete) | **DELETE** /config/custom | Delete instance runtime configuration
*ConfigApi* | [**config_custom_get**](docs/ConfigApi.md#config_custom_get) | **GET** /config/custom | Get instance runtime configuration
*ConfigApi* | [**config_custom_put**](docs/ConfigApi.md#config_custom_put) | **PUT** /config/custom | Set instance runtime configuration
*ConfigApi* | [**config_get**](docs/ConfigApi.md#config_get) | **GET** /config | Get instance public configuration
*FeedsApi* | [**feeds_video_comments_format_get**](docs/FeedsApi.md#feeds_video_comments_format_get) | **GET** /feeds/video-comments.{format} | List comments on videos
*FeedsApi* | [**feeds_videos_format_get**](docs/FeedsApi.md#feeds_videos_format_get) | **GET** /feeds/videos.{format} | List videos
*InstanceFollowsApi* | [**server_followers_get**](docs/InstanceFollowsApi.md#server_followers_get) | **GET** /server/followers | List instance followers
*InstanceFollowsApi* | [**server_following_get**](docs/InstanceFollowsApi.md#server_following_get) | **GET** /server/following | List instances followed by the server
*InstanceFollowsApi* | [**server_following_host_delete**](docs/InstanceFollowsApi.md#server_following_host_delete) | **DELETE** /server/following/{host} | Unfollow a server
*InstanceFollowsApi* | [**server_following_post**](docs/InstanceFollowsApi.md#server_following_post) | **POST** /server/following | Follow a server
*InstanceRedundancyApi* | [**redundancy_host_put**](docs/InstanceRedundancyApi.md#redundancy_host_put) | **PUT** /redundancy/{host} | Update a server redundancy policy
*JobApi* | [**jobs_state_get**](docs/JobApi.md#jobs_state_get) | **GET** /jobs/{state} | List instance jobs
*LiveVideosApi* | [**videos_live_id_get**](docs/LiveVideosApi.md#videos_live_id_get) | **GET** /videos/live/{id} | Get a live information
*LiveVideosApi* | [**videos_live_id_put**](docs/LiveVideosApi.md#videos_live_id_put) | **PUT** /videos/live/{id} | Update a live information
*LiveVideosApi* | [**videos_live_post**](docs/LiveVideosApi.md#videos_live_post) | **POST** /videos/live | Create a live
*MyNotificationsApi* | [**users_me_notification_settings_put**](docs/MyNotificationsApi.md#users_me_notification_settings_put) | **PUT** /users/me/notification-settings | Update my notification settings
*MyNotificationsApi* | [**users_me_notifications_get**](docs/MyNotificationsApi.md#users_me_notifications_get) | **GET** /users/me/notifications | List my notifications
*MyNotificationsApi* | [**users_me_notifications_read_all_post**](docs/MyNotificationsApi.md#users_me_notifications_read_all_post) | **POST** /users/me/notifications/read-all | Mark all my notification as read
*MyNotificationsApi* | [**users_me_notifications_read_post**](docs/MyNotificationsApi.md#users_me_notifications_read_post) | **POST** /users/me/notifications/read | Mark notifications as read by their id
*MySubscriptionsApi* | [**users_me_subscriptions_exist_get**](docs/MySubscriptionsApi.md#users_me_subscriptions_exist_get) | **GET** /users/me/subscriptions/exist | Get if subscriptions exist for my user
*MySubscriptionsApi* | [**users_me_subscriptions_get**](docs/MySubscriptionsApi.md#users_me_subscriptions_get) | **GET** /users/me/subscriptions | Get my user subscriptions
*MySubscriptionsApi* | [**users_me_subscriptions_post**](docs/MySubscriptionsApi.md#users_me_subscriptions_post) | **POST** /users/me/subscriptions | Add subscription to my user
*MySubscriptionsApi* | [**users_me_subscriptions_subscription_handle_delete**](docs/MySubscriptionsApi.md#users_me_subscriptions_subscription_handle_delete) | **DELETE** /users/me/subscriptions/{subscriptionHandle} | Delete subscription of my user
*MySubscriptionsApi* | [**users_me_subscriptions_subscription_handle_get**](docs/MySubscriptionsApi.md#users_me_subscriptions_subscription_handle_get) | **GET** /users/me/subscriptions/{subscriptionHandle} | Get subscription of my user
*MySubscriptionsApi* | [**users_me_subscriptions_videos_get**](docs/MySubscriptionsApi.md#users_me_subscriptions_videos_get) | **GET** /users/me/subscriptions/videos | List videos of subscriptions of my user
*MyUserApi* | [**users_me_abuses_get**](docs/MyUserApi.md#users_me_abuses_get) | **GET** /users/me/abuses | List my abuses
*MyUserApi* | [**users_me_avatar_pick_post**](docs/MyUserApi.md#users_me_avatar_pick_post) | **POST** /users/me/avatar/pick | Update my user avatar
*MyUserApi* | [**users_me_get**](docs/MyUserApi.md#users_me_get) | **GET** /users/me | Get my user information
*MyUserApi* | [**users_me_put**](docs/MyUserApi.md#users_me_put) | **PUT** /users/me | Update my user information
*MyUserApi* | [**users_me_video_quota_used_get**](docs/MyUserApi.md#users_me_video_quota_used_get) | **GET** /users/me/video-quota-used | Get my user used quota
*MyUserApi* | [**users_me_videos_get**](docs/MyUserApi.md#users_me_videos_get) | **GET** /users/me/videos | Get videos of my user
*MyUserApi* | [**users_me_videos_imports_get**](docs/MyUserApi.md#users_me_videos_imports_get) | **GET** /users/me/videos/imports | Get video imports of my user
*MyUserApi* | [**users_me_videos_video_id_rating_get**](docs/MyUserApi.md#users_me_videos_video_id_rating_get) | **GET** /users/me/videos/{videoId}/rating | Get rate of my user for a video
*PluginsApi* | [**plugins_available_get**](docs/PluginsApi.md#plugins_available_get) | **GET** /plugins/available | List available plugins
*PluginsApi* | [**plugins_get**](docs/PluginsApi.md#plugins_get) | **GET** /plugins | List plugins
*PluginsApi* | [**plugins_install_post**](docs/PluginsApi.md#plugins_install_post) | **POST** /plugins/install | Install a plugin
*PluginsApi* | [**plugins_npm_name_get**](docs/PluginsApi.md#plugins_npm_name_get) | **GET** /plugins/{npmName} | Get a plugin
*PluginsApi* | [**plugins_npm_name_public_settings_get**](docs/PluginsApi.md#plugins_npm_name_public_settings_get) | **GET** /plugins/{npmName}/public-settings | Get a plugin&#39;s public settings
*PluginsApi* | [**plugins_npm_name_registered_settings_get**](docs/PluginsApi.md#plugins_npm_name_registered_settings_get) | **GET** /plugins/{npmName}/registered-settings | Get a plugin&#39;s registered settings
*PluginsApi* | [**plugins_npm_name_settings_put**](docs/PluginsApi.md#plugins_npm_name_settings_put) | **PUT** /plugins/{npmName}/settings | Set a plugin&#39;s settings
*PluginsApi* | [**plugins_uninstall_post**](docs/PluginsApi.md#plugins_uninstall_post) | **POST** /plugins/uninstall | Uninstall a plugin
*PluginsApi* | [**plugins_update_post**](docs/PluginsApi.md#plugins_update_post) | **POST** /plugins/update | Update a plugin
*SearchApi* | [**search_video_channels_get**](docs/SearchApi.md#search_video_channels_get) | **GET** /search/video-channels | Search channels
*SearchApi* | [**search_videos_get**](docs/SearchApi.md#search_videos_get) | **GET** /search/videos | Search videos
*ServerBlocksApi* | [**blocklist_servers_get**](docs/ServerBlocksApi.md#blocklist_servers_get) | **GET** /blocklist/servers | List server blocks
*ServerBlocksApi* | [**blocklist_servers_host_delete**](docs/ServerBlocksApi.md#blocklist_servers_host_delete) | **DELETE** /blocklist/servers/{host} | Unblock a server by its domain
*ServerBlocksApi* | [**blocklist_servers_post**](docs/ServerBlocksApi.md#blocklist_servers_post) | **POST** /blocklist/servers | Block a server
*UsersApi* | [**del_user_id**](docs/UsersApi.md#del_user_id) | **DELETE** /users/{id} | Delete a user
*UsersApi* | [**get_user_id**](docs/UsersApi.md#get_user_id) | **GET** /users/{id} | Get a user
*UsersApi* | [**put_user_id**](docs/UsersApi.md#put_user_id) | **PUT** /users/{id} | Update a user
*UsersApi* | [**users_get**](docs/UsersApi.md#users_get) | **GET** /users | List users
*UsersApi* | [**users_post**](docs/UsersApi.md#users_post) | **POST** /users | Create a user
*UsersApi* | [**users_register_post**](docs/UsersApi.md#users_register_post) | **POST** /users/register | Register a user
*VideoApi* | [**accounts_name_videos_get**](docs/VideoApi.md#accounts_name_videos_get) | **GET** /accounts/{name}/videos | List videos of an account
*VideoApi* | [**video_channels_channel_handle_videos_get**](docs/VideoApi.md#video_channels_channel_handle_videos_get) | **GET** /video-channels/{channelHandle}/videos | List videos of a video channel
*VideoApi* | [**videos_categories_get**](docs/VideoApi.md#videos_categories_get) | **GET** /videos/categories | List available video categories
*VideoApi* | [**videos_get**](docs/VideoApi.md#videos_get) | **GET** /videos | List videos
*VideoApi* | [**videos_id_delete**](docs/VideoApi.md#videos_id_delete) | **DELETE** /videos/{id} | Delete a video
*VideoApi* | [**videos_id_description_get**](docs/VideoApi.md#videos_id_description_get) | **GET** /videos/{id}/description | Get complete video description
*VideoApi* | [**videos_id_get**](docs/VideoApi.md#videos_id_get) | **GET** /videos/{id} | Get a video
*VideoApi* | [**videos_id_put**](docs/VideoApi.md#videos_id_put) | **PUT** /videos/{id} | Update a video
*VideoApi* | [**videos_id_views_post**](docs/VideoApi.md#videos_id_views_post) | **POST** /videos/{id}/views | Add a view to a video
*VideoApi* | [**videos_id_watching_put**](docs/VideoApi.md#videos_id_watching_put) | **PUT** /videos/{id}/watching | Set watching progress of a video
*VideoApi* | [**videos_imports_post**](docs/VideoApi.md#videos_imports_post) | **POST** /videos/imports | Import a video
*VideoApi* | [**videos_languages_get**](docs/VideoApi.md#videos_languages_get) | **GET** /videos/languages | List available video languages
*VideoApi* | [**videos_licences_get**](docs/VideoApi.md#videos_licences_get) | **GET** /videos/licences | List available video licences
*VideoApi* | [**videos_live_id_get**](docs/VideoApi.md#videos_live_id_get) | **GET** /videos/live/{id} | Get a live information
*VideoApi* | [**videos_live_id_put**](docs/VideoApi.md#videos_live_id_put) | **PUT** /videos/live/{id} | Update a live information
*VideoApi* | [**videos_live_post**](docs/VideoApi.md#videos_live_post) | **POST** /videos/live | Create a live
*VideoApi* | [**videos_privacies_get**](docs/VideoApi.md#videos_privacies_get) | **GET** /videos/privacies | List available video privacies
*VideoApi* | [**videos_upload_post**](docs/VideoApi.md#videos_upload_post) | **POST** /videos/upload | Upload a video
*VideoBlocksApi* | [**videos_blacklist_get**](docs/VideoBlocksApi.md#videos_blacklist_get) | **GET** /videos/blacklist | List video blocks
*VideoBlocksApi* | [**videos_id_blacklist_delete**](docs/VideoBlocksApi.md#videos_id_blacklist_delete) | **DELETE** /videos/{id}/blacklist | Unblock a video by its id
*VideoBlocksApi* | [**videos_id_blacklist_post**](docs/VideoBlocksApi.md#videos_id_blacklist_post) | **POST** /videos/{id}/blacklist | Block a video
*VideoCaptionsApi* | [**videos_id_captions_caption_language_delete**](docs/VideoCaptionsApi.md#videos_id_captions_caption_language_delete) | **DELETE** /videos/{id}/captions/{captionLanguage} | Delete a video caption
*VideoCaptionsApi* | [**videos_id_captions_caption_language_put**](docs/VideoCaptionsApi.md#videos_id_captions_caption_language_put) | **PUT** /videos/{id}/captions/{captionLanguage} | Add or replace a video caption
*VideoCaptionsApi* | [**videos_id_captions_get**](docs/VideoCaptionsApi.md#videos_id_captions_get) | **GET** /videos/{id}/captions | List captions of a video
*VideoChannelsApi* | [**accounts_name_video_channels_get**](docs/VideoChannelsApi.md#accounts_name_video_channels_get) | **GET** /accounts/{name}/video-channels | List video channels of an account
*VideoChannelsApi* | [**video_channels_channel_handle_delete**](docs/VideoChannelsApi.md#video_channels_channel_handle_delete) | **DELETE** /video-channels/{channelHandle} | Delete a video channel
*VideoChannelsApi* | [**video_channels_channel_handle_get**](docs/VideoChannelsApi.md#video_channels_channel_handle_get) | **GET** /video-channels/{channelHandle} | Get a video channel
*VideoChannelsApi* | [**video_channels_channel_handle_put**](docs/VideoChannelsApi.md#video_channels_channel_handle_put) | **PUT** /video-channels/{channelHandle} | Update a video channel
*VideoChannelsApi* | [**video_channels_channel_handle_videos_get**](docs/VideoChannelsApi.md#video_channels_channel_handle_videos_get) | **GET** /video-channels/{channelHandle}/videos | List videos of a video channel
*VideoChannelsApi* | [**video_channels_get**](docs/VideoChannelsApi.md#video_channels_get) | **GET** /video-channels | List video channels
*VideoChannelsApi* | [**video_channels_post**](docs/VideoChannelsApi.md#video_channels_post) | **POST** /video-channels | Create a video channel
*VideoCommentsApi* | [**videos_id_comment_threads_get**](docs/VideoCommentsApi.md#videos_id_comment_threads_get) | **GET** /videos/{id}/comment-threads | List threads of a video
*VideoCommentsApi* | [**videos_id_comment_threads_post**](docs/VideoCommentsApi.md#videos_id_comment_threads_post) | **POST** /videos/{id}/comment-threads | Create a thread
*VideoCommentsApi* | [**videos_id_comment_threads_thread_id_get**](docs/VideoCommentsApi.md#videos_id_comment_threads_thread_id_get) | **GET** /videos/{id}/comment-threads/{threadId} | Get a thread
*VideoCommentsApi* | [**videos_id_comments_comment_id_delete**](docs/VideoCommentsApi.md#videos_id_comments_comment_id_delete) | **DELETE** /videos/{id}/comments/{commentId} | Delete a comment or a reply
*VideoCommentsApi* | [**videos_id_comments_comment_id_post**](docs/VideoCommentsApi.md#videos_id_comments_comment_id_post) | **POST** /videos/{id}/comments/{commentId} | Reply to a thread of a video
*VideoMirroringApi* | [**redundancy_videos_get**](docs/VideoMirroringApi.md#redundancy_videos_get) | **GET** /redundancy/videos | List videos being mirrored
*VideoMirroringApi* | [**redundancy_videos_post**](docs/VideoMirroringApi.md#redundancy_videos_post) | **POST** /redundancy/videos | Mirror a video
*VideoMirroringApi* | [**redundancy_videos_redundancy_id_delete**](docs/VideoMirroringApi.md#redundancy_videos_redundancy_id_delete) | **DELETE** /redundancy/videos/{redundancyId} | Delete a mirror done on a video
*VideoOwnershipChangeApi* | [**videos_id_give_ownership_post**](docs/VideoOwnershipChangeApi.md#videos_id_give_ownership_post) | **POST** /videos/{id}/give-ownership | Request ownership change
*VideoOwnershipChangeApi* | [**videos_ownership_get**](docs/VideoOwnershipChangeApi.md#videos_ownership_get) | **GET** /videos/ownership | List video ownership changes
*VideoOwnershipChangeApi* | [**videos_ownership_id_accept_post**](docs/VideoOwnershipChangeApi.md#videos_ownership_id_accept_post) | **POST** /videos/ownership/{id}/accept | Accept ownership change request
*VideoOwnershipChangeApi* | [**videos_ownership_id_refuse_post**](docs/VideoOwnershipChangeApi.md#videos_ownership_id_refuse_post) | **POST** /videos/ownership/{id}/refuse | Refuse ownership change request
*VideoPlaylistsApi* | [**users_me_video_playlists_videos_exist_get**](docs/VideoPlaylistsApi.md#users_me_video_playlists_videos_exist_get) | **GET** /users/me/video-playlists/videos-exist | Check video exists in my playlists
*VideoPlaylistsApi* | [**video_playlists_get**](docs/VideoPlaylistsApi.md#video_playlists_get) | **GET** /video-playlists | List video playlists
*VideoPlaylistsApi* | [**video_playlists_id_delete**](docs/VideoPlaylistsApi.md#video_playlists_id_delete) | **DELETE** /video-playlists/{id} | Delete a video playlist
*VideoPlaylistsApi* | [**video_playlists_id_get**](docs/VideoPlaylistsApi.md#video_playlists_id_get) | **GET** /video-playlists/{id} | Get a video playlist
*VideoPlaylistsApi* | [**video_playlists_id_put**](docs/VideoPlaylistsApi.md#video_playlists_id_put) | **PUT** /video-playlists/{id} | Update a video playlist
*VideoPlaylistsApi* | [**video_playlists_id_videos_get**](docs/VideoPlaylistsApi.md#video_playlists_id_videos_get) | **GET** /video-playlists/{id}/videos | List videos of a playlist
*VideoPlaylistsApi* | [**video_playlists_id_videos_playlist_element_id_delete**](docs/VideoPlaylistsApi.md#video_playlists_id_videos_playlist_element_id_delete) | **DELETE** /video-playlists/{id}/videos/{playlistElementId} | Delete an element from a playlist
*VideoPlaylistsApi* | [**video_playlists_id_videos_playlist_element_id_put**](docs/VideoPlaylistsApi.md#video_playlists_id_videos_playlist_element_id_put) | **PUT** /video-playlists/{id}/videos/{playlistElementId} | Update a playlist element
*VideoPlaylistsApi* | [**video_playlists_id_videos_post**](docs/VideoPlaylistsApi.md#video_playlists_id_videos_post) | **POST** /video-playlists/{id}/videos | Add a video in a playlist
*VideoPlaylistsApi* | [**video_playlists_id_videos_reorder_post**](docs/VideoPlaylistsApi.md#video_playlists_id_videos_reorder_post) | **POST** /video-playlists/{id}/videos/reorder | Reorder a playlist
*VideoPlaylistsApi* | [**video_playlists_post**](docs/VideoPlaylistsApi.md#video_playlists_post) | **POST** /video-playlists | Create a video playlist
*VideoPlaylistsApi* | [**video_playlists_privacies_get**](docs/VideoPlaylistsApi.md#video_playlists_privacies_get) | **GET** /video-playlists/privacies | List available playlist privacies
*VideoRatesApi* | [**users_me_videos_video_id_rating_get**](docs/VideoRatesApi.md#users_me_videos_video_id_rating_get) | **GET** /users/me/videos/{videoId}/rating | Get rate of my user for a video
*VideoRatesApi* | [**videos_id_rate_put**](docs/VideoRatesApi.md#videos_id_rate_put) | **PUT** /videos/{id}/rate | Like/dislike a video
*VideosApi* | [**users_me_subscriptions_videos_get**](docs/VideosApi.md#users_me_subscriptions_videos_get) | **GET** /users/me/subscriptions/videos | List videos of subscriptions of my user
*VideosApi* | [**users_me_videos_get**](docs/VideosApi.md#users_me_videos_get) | **GET** /users/me/videos | Get videos of my user
*VideosApi* | [**users_me_videos_imports_get**](docs/VideosApi.md#users_me_videos_imports_get) | **GET** /users/me/videos/imports | Get video imports of my user
*VideosApi* | [**video_playlists_id_videos_get**](docs/VideosApi.md#video_playlists_id_videos_get) | **GET** /video-playlists/{id}/videos | List videos of a playlist
*VideosApi* | [**video_playlists_id_videos_post**](docs/VideosApi.md#video_playlists_id_videos_post) | **POST** /video-playlists/{id}/videos | Add a video in a playlist


## Documentation For Models

 - [Abuse](docs/Abuse.md)
 - [AbuseMessage](docs/AbuseMessage.md)
 - [AbuseStateConstant](docs/AbuseStateConstant.md)
 - [AbuseStateSet](docs/AbuseStateSet.md)
 - [AbuseVideo](docs/AbuseVideo.md)
 - [AbusesAccount](docs/AbusesAccount.md)
 - [AbusesComment](docs/AbusesComment.md)
 - [AbusesVideo](docs/AbusesVideo.md)
 - [Account](docs/Account.md)
 - [AccountAllOf](docs/AccountAllOf.md)
 - [AccountSummary](docs/AccountSummary.md)
 - [Actor](docs/Actor.md)
 - [ActorInfo](docs/ActorInfo.md)
 - [ActorInfoAvatar](docs/ActorInfoAvatar.md)
 - [AddUser](docs/AddUser.md)
 - [AddUserResponse](docs/AddUserResponse.md)
 - [AddUserResponseUser](docs/AddUserResponseUser.md)
 - [AddUserResponseUserAccount](docs/AddUserResponseUserAccount.md)
 - [Avatar](docs/Avatar.md)
 - [CommentThreadPostResponse](docs/CommentThreadPostResponse.md)
 - [CommentThreadResponse](docs/CommentThreadResponse.md)
 - [FileRedundancyInformation](docs/FileRedundancyInformation.md)
 - [Follow](docs/Follow.md)
 - [GetMeVideoRating](docs/GetMeVideoRating.md)
 - [InlineObject](docs/InlineObject.md)
 - [InlineObject1](docs/InlineObject1.md)
 - [InlineObject10](docs/InlineObject10.md)
 - [InlineObject11](docs/InlineObject11.md)
 - [InlineObject12](docs/InlineObject12.md)
 - [InlineObject13](docs/InlineObject13.md)
 - [InlineObject14](docs/InlineObject14.md)
 - [InlineObject15](docs/InlineObject15.md)
 - [InlineObject16](docs/InlineObject16.md)
 - [InlineObject17](docs/InlineObject17.md)
 - [InlineObject18](docs/InlineObject18.md)
 - [InlineObject19](docs/InlineObject19.md)
 - [InlineObject2](docs/InlineObject2.md)
 - [InlineObject20](docs/InlineObject20.md)
 - [InlineObject21](docs/InlineObject21.md)
 - [InlineObject22](docs/InlineObject22.md)
 - [InlineObject23](docs/InlineObject23.md)
 - [InlineObject24](docs/InlineObject24.md)
 - [InlineObject25](docs/InlineObject25.md)
 - [InlineObject26](docs/InlineObject26.md)
 - [InlineObject3](docs/InlineObject3.md)
 - [InlineObject4](docs/InlineObject4.md)
 - [InlineObject5](docs/InlineObject5.md)
 - [InlineObject6](docs/InlineObject6.md)
 - [InlineObject7](docs/InlineObject7.md)
 - [InlineObject8](docs/InlineObject8.md)
 - [InlineObject9](docs/InlineObject9.md)
 - [InlineResponse200](docs/InlineResponse200.md)
 - [InlineResponse2001](docs/InlineResponse2001.md)
 - [InlineResponse2002](docs/InlineResponse2002.md)
 - [InlineResponse2003](docs/InlineResponse2003.md)
 - [InlineResponse2004](docs/InlineResponse2004.md)
 - [InlineResponse2005](docs/InlineResponse2005.md)
 - [InlineResponse2005VideoPlaylist](docs/InlineResponse2005VideoPlaylist.md)
 - [InlineResponse2006](docs/InlineResponse2006.md)
 - [InlineResponse2006VideoPlaylistElement](docs/InlineResponse2006VideoPlaylistElement.md)
 - [InlineResponse2007](docs/InlineResponse2007.md)
 - [InlineResponse2007VideoId](docs/InlineResponse2007VideoId.md)
 - [Job](docs/Job.md)
 - [LiveVideoResponse](docs/LiveVideoResponse.md)
 - [LiveVideoUpdate](docs/LiveVideoUpdate.md)
 - [MRSSGroupContent](docs/MRSSGroupContent.md)
 - [MRSSPeerLink](docs/MRSSPeerLink.md)
 - [NSFWPolicy](docs/NSFWPolicy.md)
 - [Notification](docs/Notification.md)
 - [NotificationActorFollow](docs/NotificationActorFollow.md)
 - [NotificationActorFollowFollowing](docs/NotificationActorFollowFollowing.md)
 - [NotificationComment](docs/NotificationComment.md)
 - [NotificationListResponse](docs/NotificationListResponse.md)
 - [NotificationSettingValue](docs/NotificationSettingValue.md)
 - [NotificationVideoAbuse](docs/NotificationVideoAbuse.md)
 - [NotificationVideoImport](docs/NotificationVideoImport.md)
 - [PlaylistElement](docs/PlaylistElement.md)
 - [Plugin](docs/Plugin.md)
 - [PluginResponse](docs/PluginResponse.md)
 - [RegisterUser](docs/RegisterUser.md)
 - [RegisterUserChannel](docs/RegisterUserChannel.md)
 - [ServerConfig](docs/ServerConfig.md)
 - [ServerConfigAbout](docs/ServerConfigAbout.md)
 - [ServerConfigAboutInstance](docs/ServerConfigAboutInstance.md)
 - [ServerConfigAutoBlacklist](docs/ServerConfigAutoBlacklist.md)
 - [ServerConfigAutoBlacklistVideos](docs/ServerConfigAutoBlacklistVideos.md)
 - [ServerConfigAvatar](docs/ServerConfigAvatar.md)
 - [ServerConfigAvatarFile](docs/ServerConfigAvatarFile.md)
 - [ServerConfigAvatarFileSize](docs/ServerConfigAvatarFileSize.md)
 - [ServerConfigCustom](docs/ServerConfigCustom.md)
 - [ServerConfigCustomAdmin](docs/ServerConfigCustomAdmin.md)
 - [ServerConfigCustomCache](docs/ServerConfigCustomCache.md)
 - [ServerConfigCustomCachePreviews](docs/ServerConfigCustomCachePreviews.md)
 - [ServerConfigCustomFollowers](docs/ServerConfigCustomFollowers.md)
 - [ServerConfigCustomFollowersInstance](docs/ServerConfigCustomFollowersInstance.md)
 - [ServerConfigCustomInstance](docs/ServerConfigCustomInstance.md)
 - [ServerConfigCustomServices](docs/ServerConfigCustomServices.md)
 - [ServerConfigCustomServicesTwitter](docs/ServerConfigCustomServicesTwitter.md)
 - [ServerConfigCustomSignup](docs/ServerConfigCustomSignup.md)
 - [ServerConfigCustomTheme](docs/ServerConfigCustomTheme.md)
 - [ServerConfigCustomTranscoding](docs/ServerConfigCustomTranscoding.md)
 - [ServerConfigCustomTranscodingResolutions](docs/ServerConfigCustomTranscodingResolutions.md)
 - [ServerConfigEmail](docs/ServerConfigEmail.md)
 - [ServerConfigFollowings](docs/ServerConfigFollowings.md)
 - [ServerConfigFollowingsInstance](docs/ServerConfigFollowingsInstance.md)
 - [ServerConfigFollowingsInstanceAutoFollowIndex](docs/ServerConfigFollowingsInstanceAutoFollowIndex.md)
 - [ServerConfigImport](docs/ServerConfigImport.md)
 - [ServerConfigImportVideos](docs/ServerConfigImportVideos.md)
 - [ServerConfigInstance](docs/ServerConfigInstance.md)
 - [ServerConfigInstanceCustomizations](docs/ServerConfigInstanceCustomizations.md)
 - [ServerConfigPlugin](docs/ServerConfigPlugin.md)
 - [ServerConfigSearch](docs/ServerConfigSearch.md)
 - [ServerConfigSearchRemoteUri](docs/ServerConfigSearchRemoteUri.md)
 - [ServerConfigSignup](docs/ServerConfigSignup.md)
 - [ServerConfigTranscoding](docs/ServerConfigTranscoding.md)
 - [ServerConfigTrending](docs/ServerConfigTrending.md)
 - [ServerConfigTrendingVideos](docs/ServerConfigTrendingVideos.md)
 - [ServerConfigUser](docs/ServerConfigUser.md)
 - [ServerConfigVideo](docs/ServerConfigVideo.md)
 - [ServerConfigVideoCaption](docs/ServerConfigVideoCaption.md)
 - [ServerConfigVideoCaptionFile](docs/ServerConfigVideoCaptionFile.md)
 - [ServerConfigVideoFile](docs/ServerConfigVideoFile.md)
 - [ServerConfigVideoImage](docs/ServerConfigVideoImage.md)
 - [UpdateMe](docs/UpdateMe.md)
 - [UpdateUser](docs/UpdateUser.md)
 - [User](docs/User.md)
 - [UserRole](docs/UserRole.md)
 - [UserWatchingVideo](docs/UserWatchingVideo.md)
 - [Video](docs/Video.md)
 - [VideoBlacklist](docs/VideoBlacklist.md)
 - [VideoCaption](docs/VideoCaption.md)
 - [VideoChannel](docs/VideoChannel.md)
 - [VideoChannelCreate](docs/VideoChannelCreate.md)
 - [VideoChannelOwnerAccount](docs/VideoChannelOwnerAccount.md)
 - [VideoChannelSummary](docs/VideoChannelSummary.md)
 - [VideoChannelUpdate](docs/VideoChannelUpdate.md)
 - [VideoComment](docs/VideoComment.md)
 - [VideoCommentThreadTree](docs/VideoCommentThreadTree.md)
 - [VideoConstantNumber](docs/VideoConstantNumber.md)
 - [VideoConstantString](docs/VideoConstantString.md)
 - [VideoDetails](docs/VideoDetails.md)
 - [VideoDetailsAllOf](docs/VideoDetailsAllOf.md)
 - [VideoFile](docs/VideoFile.md)
 - [VideoImport](docs/VideoImport.md)
 - [VideoImportStateConstant](docs/VideoImportStateConstant.md)
 - [VideoInfo](docs/VideoInfo.md)
 - [VideoListResponse](docs/VideoListResponse.md)
 - [VideoPlaylist](docs/VideoPlaylist.md)
 - [VideoPlaylistPrivacyConstant](docs/VideoPlaylistPrivacyConstant.md)
 - [VideoPlaylistPrivacySet](docs/VideoPlaylistPrivacySet.md)
 - [VideoPlaylistTypeConstant](docs/VideoPlaylistTypeConstant.md)
 - [VideoPlaylistTypeSet](docs/VideoPlaylistTypeSet.md)
 - [VideoPrivacyConstant](docs/VideoPrivacyConstant.md)
 - [VideoPrivacySet](docs/VideoPrivacySet.md)
 - [VideoRating](docs/VideoRating.md)
 - [VideoRedundancy](docs/VideoRedundancy.md)
 - [VideoRedundancyRedundancies](docs/VideoRedundancyRedundancies.md)
 - [VideoResolutionConstant](docs/VideoResolutionConstant.md)
 - [VideoScheduledUpdate](docs/VideoScheduledUpdate.md)
 - [VideoStateConstant](docs/VideoStateConstant.md)
 - [VideoStreamingPlaylists](docs/VideoStreamingPlaylists.md)
 - [VideoStreamingPlaylistsRedundancies](docs/VideoStreamingPlaylistsRedundancies.md)
 - [VideoUploadResponse](docs/VideoUploadResponse.md)
 - [VideoUploadResponseVideo](docs/VideoUploadResponseVideo.md)
 - [VideoUserHistory](docs/VideoUserHistory.md)


## Documentation For Authorization


## OAuth2

- **Type**: OAuth
- **Flow**: password
- **Authorization URL**: 
- **Scopes**: 
 - **admin**: Admin scope
 - **moderator**: Moderator scope
 - **user**: User scope


## Author





## License

Copyright (C) 2015-2020 PeerTube Contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see http://www.gnu.org/licenses.
