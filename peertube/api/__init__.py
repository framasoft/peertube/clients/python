from __future__ import absolute_import

# flake8: noqa

# import apis into api package
from peertube.api.abuses_api import AbusesApi
from peertube.api.account_blocks_api import AccountBlocksApi
from peertube.api.accounts_api import AccountsApi
from peertube.api.config_api import ConfigApi
from peertube.api.feeds_api import FeedsApi
from peertube.api.instance_follows_api import InstanceFollowsApi
from peertube.api.instance_redundancy_api import InstanceRedundancyApi
from peertube.api.job_api import JobApi
from peertube.api.live_videos_api import LiveVideosApi
from peertube.api.my_notifications_api import MyNotificationsApi
from peertube.api.my_subscriptions_api import MySubscriptionsApi
from peertube.api.my_user_api import MyUserApi
from peertube.api.plugins_api import PluginsApi
from peertube.api.search_api import SearchApi
from peertube.api.server_blocks_api import ServerBlocksApi
from peertube.api.users_api import UsersApi
from peertube.api.video_api import VideoApi
from peertube.api.video_blocks_api import VideoBlocksApi
from peertube.api.video_captions_api import VideoCaptionsApi
from peertube.api.video_channels_api import VideoChannelsApi
from peertube.api.video_comments_api import VideoCommentsApi
from peertube.api.video_mirroring_api import VideoMirroringApi
from peertube.api.video_ownership_change_api import VideoOwnershipChangeApi
from peertube.api.video_playlists_api import VideoPlaylistsApi
from peertube.api.video_rates_api import VideoRatesApi
from peertube.api.videos_api import VideosApi
