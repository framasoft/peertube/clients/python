# PlaylistElement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**position** | **int** |  | [optional] 
**start_timestamp** | **int** |  | [optional] 
**stop_timestamp** | **int** |  | [optional] 
**video** | [**Video**](Video.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


