# UpdateUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The user id | 
**email** | **str** | The updated email of the user | 
**video_quota** | **int** | The updated video quota of the user | 
**video_quota_daily** | **int** | The updated daily video quota of the user | 
**role** | [**UserRole**](UserRole.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


