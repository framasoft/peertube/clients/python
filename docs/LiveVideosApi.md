# peertube.LiveVideosApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videos_live_id_get**](LiveVideosApi.md#videos_live_id_get) | **GET** /videos/live/{id} | Get a live information
[**videos_live_id_put**](LiveVideosApi.md#videos_live_id_put) | **PUT** /videos/live/{id} | Update a live information
[**videos_live_post**](LiveVideosApi.md#videos_live_post) | **POST** /videos/live | Create a live


# **videos_live_id_get**
> LiveVideoResponse videos_live_id_get(id)

Get a live information

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.LiveVideosApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid

    try:
        # Get a live information
        api_response = api_instance.videos_live_id_get(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LiveVideosApi->videos_live_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

[**LiveVideoResponse**](LiveVideoResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_live_id_put**
> videos_live_id_put(id, live_video_update=live_video_update)

Update a live information

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.LiveVideosApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid
live_video_update = peertube.LiveVideoUpdate() # LiveVideoUpdate |  (optional)

    try:
        # Update a live information
        api_instance.videos_live_id_put(id, live_video_update=live_video_update)
    except ApiException as e:
        print("Exception when calling LiveVideosApi->videos_live_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **live_video_update** | [**LiveVideoUpdate**](LiveVideoUpdate.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful operation |  -  |
**400** | Bad parameters or trying to update a live that has already started |  -  |
**403** | Trying to save replay of the live but saving replay is not enabled on the instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_live_post**
> VideoUploadResponse videos_live_post(channel_id, name, save_replay=save_replay, thumbnailfile=thumbnailfile, previewfile=previewfile, privacy=privacy, category=category, licence=licence, language=language, description=description, support=support, nsfw=nsfw, tags=tags, comments_enabled=comments_enabled, download_enabled=download_enabled)

Create a live

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.LiveVideosApi(api_client)
    channel_id = 56 # int | Channel id that will contain this live video
name = 'name_example' # str | Live video/replay name
save_replay = True # bool |  (optional)
thumbnailfile = '/path/to/file' # file | Live video/replay thumbnail file (optional)
previewfile = '/path/to/file' # file | Live video/replay preview file (optional)
privacy = peertube.VideoPrivacySet() # VideoPrivacySet |  (optional)
category = 'category_example' # str | Live video/replay category (optional)
licence = 'licence_example' # str | Live video/replay licence (optional)
language = 'language_example' # str | Live video/replay language (optional)
description = 'description_example' # str | Live video/replay description (optional)
support = 'support_example' # str | A text tell the audience how to support the creator (optional)
nsfw = True # bool | Whether or not this live video/replay contains sensitive content (optional)
tags = 'tags_example' # list[str] | Live video/replay tags (maximum 5 tags each between 2 and 30 characters) (optional)
comments_enabled = True # bool | Enable or disable comments for this live video/replay (optional)
download_enabled = True # bool | Enable or disable downloading for the replay of this live (optional)

    try:
        # Create a live
        api_response = api_instance.videos_live_post(channel_id, name, save_replay=save_replay, thumbnailfile=thumbnailfile, previewfile=previewfile, privacy=privacy, category=category, licence=licence, language=language, description=description, support=support, nsfw=nsfw, tags=tags, comments_enabled=comments_enabled, download_enabled=download_enabled)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling LiveVideosApi->videos_live_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_id** | **int**| Channel id that will contain this live video | 
 **name** | **str**| Live video/replay name | 
 **save_replay** | **bool**|  | [optional] 
 **thumbnailfile** | **file**| Live video/replay thumbnail file | [optional] 
 **previewfile** | **file**| Live video/replay preview file | [optional] 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] 
 **category** | **str**| Live video/replay category | [optional] 
 **licence** | **str**| Live video/replay licence | [optional] 
 **language** | **str**| Live video/replay language | [optional] 
 **description** | **str**| Live video/replay description | [optional] 
 **support** | **str**| A text tell the audience how to support the creator | [optional] 
 **nsfw** | **bool**| Whether or not this live video/replay contains sensitive content | [optional] 
 **tags** | [**list[str]**](str.md)| Live video/replay tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
 **comments_enabled** | **bool**| Enable or disable comments for this live video/replay | [optional] 
 **download_enabled** | **bool**| Enable or disable downloading for the replay of this live | [optional] 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**403** | Live is not enabled, allow replay is not enabled, or max instance/user live videos limit is exceeded |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

