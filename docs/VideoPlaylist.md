# VideoPlaylist

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**updated_at** | **datetime** |  | [optional] 
**description** | **str** |  | [optional] 
**uuid** | **str** |  | [optional] 
**display_name** | **str** |  | [optional] 
**is_local** | **bool** |  | [optional] 
**video_length** | **int** |  | [optional] 
**thumbnail_path** | **str** |  | [optional] 
**privacy** | [**VideoPlaylistPrivacyConstant**](VideoPlaylistPrivacyConstant.md) |  | [optional] 
**type** | [**VideoPlaylistTypeConstant**](VideoPlaylistTypeConstant.md) |  | [optional] 
**owner_account** | [**AccountSummary**](AccountSummary.md) |  | [optional] 
**video_channel** | [**VideoChannelSummary**](VideoChannelSummary.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


