# AbusesVideo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **float** | Video id to report | [optional] 
**start_at** | **int** | Timestamp in the video that marks the beginning of the report | [optional] 
**end_at** | **int** | Timestamp in the video that marks the ending of the report | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


