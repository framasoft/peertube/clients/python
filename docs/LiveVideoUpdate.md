# LiveVideoUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**save_replay** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


