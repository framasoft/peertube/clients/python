# ServerConfigFollowingsInstance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_follow_index** | [**ServerConfigFollowingsInstanceAutoFollowIndex**](ServerConfigFollowingsInstanceAutoFollowIndex.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


