# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] [readonly] 
**username** | **str** | The user username | [optional] 
**email** | **str** | The user email | [optional] 
**theme** | **str** | Theme enabled by this user | [optional] 
**email_verified** | **bool** | Has the user confirmed their email address? | [optional] 
**nsfw_policy** | [**NSFWPolicy**](NSFWPolicy.md) |  | [optional] 
**webtorrent_enabled** | **bool** | Enable P2P in the player | [optional] 
**auto_play_video** | **bool** | Automatically start playing the video on the watch page | [optional] 
**role** | [**UserRole**](UserRole.md) |  | [optional] 
**role_label** | **str** |  | [optional] 
**video_quota** | **int** | The user video quota | [optional] 
**video_quota_daily** | **int** | The user daily video quota | [optional] 
**videos_count** | **int** |  | [optional] 
**abuses_count** | **int** |  | [optional] 
**abuses_accepted_count** | **int** |  | [optional] 
**abuses_created_count** | **int** |  | [optional] 
**video_comments_count** | **int** |  | [optional] 
**no_instance_config_warning_modal** | **bool** |  | [optional] 
**no_welcome_modal** | **bool** |  | [optional] 
**blocked** | **bool** |  | [optional] 
**blocked_reason** | **str** |  | [optional] 
**created_at** | **str** |  | [optional] 
**account** | [**Account**](Account.md) |  | [optional] 
**video_channels** | [**list[VideoChannel]**](VideoChannel.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


