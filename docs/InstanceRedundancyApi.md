# peertube.InstanceRedundancyApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**redundancy_host_put**](InstanceRedundancyApi.md#redundancy_host_put) | **PUT** /redundancy/{host} | Update a server redundancy policy


# **redundancy_host_put**
> redundancy_host_put(host, inline_object23=inline_object23)

Update a server redundancy policy

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.InstanceRedundancyApi(api_client)
    host = 'host_example' # str | server domain to mirror
inline_object23 = peertube.InlineObject23() # InlineObject23 |  (optional)

    try:
        # Update a server redundancy policy
        api_instance.redundancy_host_put(host, inline_object23=inline_object23)
    except ApiException as e:
        print("Exception when calling InstanceRedundancyApi->redundancy_host_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host** | **str**| server domain to mirror | 
 **inline_object23** | [**InlineObject23**](InlineObject23.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**404** | server is not already known |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

