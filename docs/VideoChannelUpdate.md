# VideoChannelUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**support** | **str** | A text shown by default on all videos of this channel, to tell the audience how to support it | [optional] 
**bulk_videos_support_update** | **bool** | Update the support field for all videos of this channel | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


