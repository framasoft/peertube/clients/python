# peertube.AccountsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accounts_get**](AccountsApi.md#accounts_get) | **GET** /accounts | List accounts
[**accounts_name_get**](AccountsApi.md#accounts_name_get) | **GET** /accounts/{name} | Get an account
[**accounts_name_ratings_get**](AccountsApi.md#accounts_name_ratings_get) | **GET** /accounts/{name}/ratings | List ratings of an account
[**accounts_name_video_channels_get**](AccountsApi.md#accounts_name_video_channels_get) | **GET** /accounts/{name}/video-channels | List video channels of an account
[**accounts_name_videos_get**](AccountsApi.md#accounts_name_videos_get) | **GET** /accounts/{name}/videos | List videos of an account


# **accounts_get**
> list[Account] accounts_get(start=start, count=count, sort=sort)

List accounts

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.AccountsApi(api_client)
    start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = '-createdAt' # str | Sort column (optional)

    try:
        # List accounts
        api_response = api_instance.accounts_get(start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccountsApi->accounts_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort column | [optional] 

### Return type

[**list[Account]**](Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **accounts_name_get**
> Account accounts_name_get(name)

Get an account

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.AccountsApi(api_client)
    name = 'chocobozzz | chocobozzz@example.org' # str | The name of the account

    try:
        # Get an account
        api_response = api_instance.accounts_name_get(name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccountsApi->accounts_name_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| The name of the account | 

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | account not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **accounts_name_ratings_get**
> list[VideoRating] accounts_name_ratings_get(name, start=start, count=count, sort=sort, rating=rating)

List ratings of an account

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.AccountsApi(api_client)
    name = 'chocobozzz | chocobozzz@example.org' # str | The name of the account
start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = '-createdAt' # str | Sort column (optional)
rating = 'rating_example' # str | Optionally filter which ratings to retrieve (optional)

    try:
        # List ratings of an account
        api_response = api_instance.accounts_name_ratings_get(name, start=start, count=count, sort=sort, rating=rating)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccountsApi->accounts_name_ratings_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| The name of the account | 
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort column | [optional] 
 **rating** | **str**| Optionally filter which ratings to retrieve | [optional] 

### Return type

[**list[VideoRating]**](VideoRating.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **accounts_name_video_channels_get**
> list[VideoChannel] accounts_name_video_channels_get(name, with_stats=with_stats, start=start, count=count, sort=sort)

List video channels of an account

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.AccountsApi(api_client)
    name = 'chocobozzz | chocobozzz@example.org' # str | The name of the account
with_stats = True # bool | include view statistics for the last 30 days (only if authentified as the account user) (optional)
start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = '-createdAt' # str | Sort column (optional)

    try:
        # List video channels of an account
        api_response = api_instance.accounts_name_video_channels_get(name, with_stats=with_stats, start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccountsApi->accounts_name_video_channels_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| The name of the account | 
 **with_stats** | **bool**| include view statistics for the last 30 days (only if authentified as the account user) | [optional] 
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort column | [optional] 

### Return type

[**list[VideoChannel]**](VideoChannel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **accounts_name_videos_get**
> VideoListResponse accounts_name_videos_get(name, category_one_of=category_one_of, tags_one_of=tags_one_of, tags_all_of=tags_all_of, licence_one_of=licence_one_of, language_one_of=language_one_of, nsfw=nsfw, filter=filter, skip_count=skip_count, start=start, count=count, sort=sort)

List videos of an account

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.AccountsApi(api_client)
    name = 'chocobozzz | chocobozzz@example.org' # str | The name of the account
category_one_of = peertube.OneOfintegerarray() # OneOfintegerarray | category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) (optional)
tags_one_of = peertube.OneOfstringarray() # OneOfstringarray | tag(s) of the video (optional)
tags_all_of = peertube.OneOfstringarray() # OneOfstringarray | tag(s) of the video, where all should be present in the video (optional)
licence_one_of = peertube.OneOfintegerarray() # OneOfintegerarray | licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) (optional)
language_one_of = peertube.OneOfstringarray() # OneOfstringarray | language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use `_unknown` to filter on videos that don't have a video language (optional)
nsfw = 'nsfw_example' # str | whether to include nsfw videos, if any (optional)
filter = 'filter_example' # str | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
skip_count = 'false' # str | if you don't need the `total` in the response (optional) (default to 'false')
start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = 'sort_example' # str | Sort videos by criteria (optional)

    try:
        # List videos of an account
        api_response = api_instance.accounts_name_videos_get(name, category_one_of=category_one_of, tags_one_of=tags_one_of, tags_all_of=tags_all_of, licence_one_of=licence_one_of, language_one_of=language_one_of, nsfw=nsfw, filter=filter, skip_count=skip_count, start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AccountsApi->accounts_name_videos_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| The name of the account | 
 **category_one_of** | [**OneOfintegerarray**](.md)| category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) | [optional] 
 **tags_one_of** | [**OneOfstringarray**](.md)| tag(s) of the video | [optional] 
 **tags_all_of** | [**OneOfstringarray**](.md)| tag(s) of the video, where all should be present in the video | [optional] 
 **licence_one_of** | [**OneOfintegerarray**](.md)| licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) | [optional] 
 **language_one_of** | [**OneOfstringarray**](.md)| language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional] 
 **nsfw** | **str**| whether to include nsfw videos, if any | [optional] 
 **filter** | **str**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] 
 **skip_count** | **str**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to &#39;false&#39;]
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort videos by criteria | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

