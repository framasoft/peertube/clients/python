# UserWatchingVideo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_time** | **int** | timestamp within the video, in seconds | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


