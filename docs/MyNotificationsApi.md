# peertube.MyNotificationsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**users_me_notification_settings_put**](MyNotificationsApi.md#users_me_notification_settings_put) | **PUT** /users/me/notification-settings | Update my notification settings
[**users_me_notifications_get**](MyNotificationsApi.md#users_me_notifications_get) | **GET** /users/me/notifications | List my notifications
[**users_me_notifications_read_all_post**](MyNotificationsApi.md#users_me_notifications_read_all_post) | **POST** /users/me/notifications/read-all | Mark all my notification as read
[**users_me_notifications_read_post**](MyNotificationsApi.md#users_me_notifications_read_post) | **POST** /users/me/notifications/read | Mark notifications as read by their id


# **users_me_notification_settings_put**
> users_me_notification_settings_put(inline_object3=inline_object3)

Update my notification settings

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyNotificationsApi(api_client)
    inline_object3 = peertube.InlineObject3() # InlineObject3 |  (optional)

    try:
        # Update my notification settings
        api_instance.users_me_notification_settings_put(inline_object3=inline_object3)
    except ApiException as e:
        print("Exception when calling MyNotificationsApi->users_me_notification_settings_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object3** | [**InlineObject3**](InlineObject3.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_notifications_get**
> NotificationListResponse users_me_notifications_get(unread=unread, start=start, count=count, sort=sort)

List my notifications

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyNotificationsApi(api_client)
    unread = True # bool | only list unread notifications (optional)
start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = '-createdAt' # str | Sort column (optional)

    try:
        # List my notifications
        api_response = api_instance.users_me_notifications_get(unread=unread, start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling MyNotificationsApi->users_me_notifications_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unread** | **bool**| only list unread notifications | [optional] 
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort column | [optional] 

### Return type

[**NotificationListResponse**](NotificationListResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_notifications_read_all_post**
> users_me_notifications_read_all_post()

Mark all my notification as read

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyNotificationsApi(api_client)
    
    try:
        # Mark all my notification as read
        api_instance.users_me_notifications_read_all_post()
    except ApiException as e:
        print("Exception when calling MyNotificationsApi->users_me_notifications_read_all_post: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_notifications_read_post**
> users_me_notifications_read_post(inline_object2=inline_object2)

Mark notifications as read by their id

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MyNotificationsApi(api_client)
    inline_object2 = peertube.InlineObject2() # InlineObject2 |  (optional)

    try:
        # Mark notifications as read by their id
        api_instance.users_me_notifications_read_post(inline_object2=inline_object2)
    except ApiException as e:
        print("Exception when calling MyNotificationsApi->users_me_notifications_read_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object2** | [**InlineObject2**](InlineObject2.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

