# Plugin

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**type** | **int** | - &#x60;1&#x60;: PLUGIN - &#x60;2&#x60;: THEME  | [optional] 
**latest_version** | **str** |  | [optional] 
**version** | **str** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**uninstalled** | **bool** |  | [optional] 
**peertube_engine** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**homepage** | **str** |  | [optional] 
**settings** | **dict(str, object)** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**updated_at** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


