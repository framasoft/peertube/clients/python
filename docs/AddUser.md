# AddUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **str** | The user username | 
**password** | **str** | The user password. If the smtp server is configured, you can leave empty and an email will be sent | 
**email** | **str** | The user email | 
**video_quota** | **int** | The user video quota | 
**video_quota_daily** | **int** | The user daily video quota | 
**role** | [**UserRole**](UserRole.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


