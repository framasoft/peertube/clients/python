# peertube.FeedsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**feeds_video_comments_format_get**](FeedsApi.md#feeds_video_comments_format_get) | **GET** /feeds/video-comments.{format} | List comments on videos
[**feeds_videos_format_get**](FeedsApi.md#feeds_videos_format_get) | **GET** /feeds/videos.{format} | List videos


# **feeds_video_comments_format_get**
> list[object] feeds_video_comments_format_get(format, video_id=video_id, account_id=account_id, account_name=account_name, video_channel_id=video_channel_id, video_channel_name=video_channel_name)

List comments on videos

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.FeedsApi(api_client)
    format = 'format_example' # str | format expected (we focus on making `rss` the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss))
video_id = 'video_id_example' # str | limit listing to a specific video (optional)
account_id = 'account_id_example' # str | limit listing to a specific account (optional)
account_name = 'account_name_example' # str | limit listing to a specific account (optional)
video_channel_id = 'video_channel_id_example' # str | limit listing to a specific video channel (optional)
video_channel_name = 'video_channel_name_example' # str | limit listing to a specific video channel (optional)

    try:
        # List comments on videos
        api_response = api_instance.feeds_video_comments_format_get(format, video_id=video_id, account_id=account_id, account_name=account_name, video_channel_id=video_channel_id, video_channel_name=video_channel_name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling FeedsApi->feeds_video_comments_format_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **str**| format expected (we focus on making &#x60;rss&#x60; the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss)) | 
 **video_id** | **str**| limit listing to a specific video | [optional] 
 **account_id** | **str**| limit listing to a specific account | [optional] 
 **account_name** | **str**| limit listing to a specific account | [optional] 
 **video_channel_id** | **str**| limit listing to a specific video channel | [optional] 
 **video_channel_name** | **str**| limit listing to a specific video channel | [optional] 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/rss+xml, text/xml, application/atom+xml, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  * Cache-Control -  <br>  |
**400** | Arises when:   - videoId filter is mixed with a channel filter  |  -  |
**404** | video, video channel or account not found |  -  |
**406** | accept header unsupported |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **feeds_videos_format_get**
> list[object] feeds_videos_format_get(format, account_id=account_id, account_name=account_name, video_channel_id=video_channel_id, video_channel_name=video_channel_name, sort=sort, nsfw=nsfw, filter=filter)

List videos

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.FeedsApi(api_client)
    format = 'format_example' # str | format expected (we focus on making `rss` the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss))
account_id = 'account_id_example' # str | limit listing to a specific account (optional)
account_name = 'account_name_example' # str | limit listing to a specific account (optional)
video_channel_id = 'video_channel_id_example' # str | limit listing to a specific video channel (optional)
video_channel_name = 'video_channel_name_example' # str | limit listing to a specific video channel (optional)
sort = '-createdAt' # str | Sort column (optional)
nsfw = 'nsfw_example' # str | whether to include nsfw videos, if any (optional)
filter = 'filter_example' # str | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)

    try:
        # List videos
        api_response = api_instance.feeds_videos_format_get(format, account_id=account_id, account_name=account_name, video_channel_id=video_channel_id, video_channel_name=video_channel_name, sort=sort, nsfw=nsfw, filter=filter)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling FeedsApi->feeds_videos_format_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **str**| format expected (we focus on making &#x60;rss&#x60; the most featureful ; it serves [Media RSS](https://www.rssboard.org/media-rss)) | 
 **account_id** | **str**| limit listing to a specific account | [optional] 
 **account_name** | **str**| limit listing to a specific account | [optional] 
 **video_channel_id** | **str**| limit listing to a specific video channel | [optional] 
 **video_channel_name** | **str**| limit listing to a specific video channel | [optional] 
 **sort** | **str**| Sort column | [optional] 
 **nsfw** | **str**| whether to include nsfw videos, if any | [optional] 
 **filter** | **str**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] 

### Return type

**list[object]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/xml, application/rss+xml, text/xml, application/atom+xml, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  * Cache-Control -  <br>  |
**404** | video channel or account not found |  -  |
**406** | accept header unsupported |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

