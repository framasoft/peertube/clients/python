# InlineObject11

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state** | [**AbuseStateSet**](AbuseStateSet.md) |  | [optional] 
**moderation_comment** | **str** | Update the report comment visible only to the moderation team | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


