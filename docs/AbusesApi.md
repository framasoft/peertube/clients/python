# peertube.AbusesApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**abuses_abuse_id_delete**](AbusesApi.md#abuses_abuse_id_delete) | **DELETE** /abuses/{abuseId} | Delete an abuse
[**abuses_abuse_id_messages_abuse_message_id_delete**](AbusesApi.md#abuses_abuse_id_messages_abuse_message_id_delete) | **DELETE** /abuses/{abuseId}/messages/{abuseMessageId} | Delete an abuse message
[**abuses_abuse_id_messages_get**](AbusesApi.md#abuses_abuse_id_messages_get) | **GET** /abuses/{abuseId}/messages | List messages of an abuse
[**abuses_abuse_id_messages_post**](AbusesApi.md#abuses_abuse_id_messages_post) | **POST** /abuses/{abuseId}/messages | Add message to an abuse
[**abuses_abuse_id_put**](AbusesApi.md#abuses_abuse_id_put) | **PUT** /abuses/{abuseId} | Update an abuse
[**abuses_get**](AbusesApi.md#abuses_get) | **GET** /abuses | List abuses
[**abuses_post**](AbusesApi.md#abuses_post) | **POST** /abuses | Report an abuse
[**users_me_abuses_get**](AbusesApi.md#users_me_abuses_get) | **GET** /users/me/abuses | List my abuses


# **abuses_abuse_id_delete**
> abuses_abuse_id_delete(abuse_id)

Delete an abuse

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.AbusesApi(api_client)
    abuse_id = 56 # int | Abuse id

    try:
        # Delete an abuse
        api_instance.abuses_abuse_id_delete(abuse_id)
    except ApiException as e:
        print("Exception when calling AbusesApi->abuses_abuse_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **abuse_id** | **int**| Abuse id | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**404** | block not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **abuses_abuse_id_messages_abuse_message_id_delete**
> abuses_abuse_id_messages_abuse_message_id_delete(abuse_id, abuse_message_id)

Delete an abuse message

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.AbusesApi(api_client)
    abuse_id = 56 # int | Abuse id
abuse_message_id = 56 # int | Abuse message id

    try:
        # Delete an abuse message
        api_instance.abuses_abuse_id_messages_abuse_message_id_delete(abuse_id, abuse_message_id)
    except ApiException as e:
        print("Exception when calling AbusesApi->abuses_abuse_id_messages_abuse_message_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **abuse_id** | **int**| Abuse id | 
 **abuse_message_id** | **int**| Abuse message id | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **abuses_abuse_id_messages_get**
> list[AbuseMessage] abuses_abuse_id_messages_get(abuse_id)

List messages of an abuse

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.AbusesApi(api_client)
    abuse_id = 56 # int | Abuse id

    try:
        # List messages of an abuse
        api_response = api_instance.abuses_abuse_id_messages_get(abuse_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AbusesApi->abuses_abuse_id_messages_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **abuse_id** | **int**| Abuse id | 

### Return type

[**list[AbuseMessage]**](AbuseMessage.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **abuses_abuse_id_messages_post**
> abuses_abuse_id_messages_post(abuse_id, inline_object12)

Add message to an abuse

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.AbusesApi(api_client)
    abuse_id = 56 # int | Abuse id
inline_object12 = peertube.InlineObject12() # InlineObject12 | 

    try:
        # Add message to an abuse
        api_instance.abuses_abuse_id_messages_post(abuse_id, inline_object12)
    except ApiException as e:
        print("Exception when calling AbusesApi->abuses_abuse_id_messages_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **abuse_id** | **int**| Abuse id | 
 **inline_object12** | [**InlineObject12**](InlineObject12.md)|  | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | incorrect request parameters |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **abuses_abuse_id_put**
> abuses_abuse_id_put(abuse_id, inline_object11=inline_object11)

Update an abuse

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.AbusesApi(api_client)
    abuse_id = 56 # int | Abuse id
inline_object11 = peertube.InlineObject11() # InlineObject11 |  (optional)

    try:
        # Update an abuse
        api_instance.abuses_abuse_id_put(abuse_id, inline_object11=inline_object11)
    except ApiException as e:
        print("Exception when calling AbusesApi->abuses_abuse_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **abuse_id** | **int**| Abuse id | 
 **inline_object11** | [**InlineObject11**](InlineObject11.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**404** | abuse not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **abuses_get**
> list[Abuse] abuses_get(id=id, predefined_reason=predefined_reason, search=search, state=state, search_reporter=search_reporter, search_reportee=search_reportee, search_video=search_video, search_video_channel=search_video_channel, video_is=video_is, filter=filter, start=start, count=count, sort=sort)

List abuses

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.AbusesApi(api_client)
    id = 56 # int | only list the report with this id (optional)
predefined_reason = ['predefined_reason_example'] # list[str] | predefined reason the listed reports should contain (optional)
search = 'search_example' # str | plain search that will match with video titles, reporter names and more (optional)
state = peertube.AbuseStateSet() # AbuseStateSet |  (optional)
search_reporter = 'search_reporter_example' # str | only list reports of a specific reporter (optional)
search_reportee = 'search_reportee_example' # str | only list reports of a specific reportee (optional)
search_video = 'search_video_example' # str | only list reports of a specific video (optional)
search_video_channel = 'search_video_channel_example' # str | only list reports of a specific video channel (optional)
video_is = 'video_is_example' # str | only list blacklisted or deleted videos (optional)
filter = 'filter_example' # str | only list account, comment or video reports (optional)
start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = 'sort_example' # str | Sort abuses by criteria (optional)

    try:
        # List abuses
        api_response = api_instance.abuses_get(id=id, predefined_reason=predefined_reason, search=search, state=state, search_reporter=search_reporter, search_reportee=search_reportee, search_video=search_video, search_video_channel=search_video_channel, video_is=video_is, filter=filter, start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AbusesApi->abuses_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| only list the report with this id | [optional] 
 **predefined_reason** | [**list[str]**](str.md)| predefined reason the listed reports should contain | [optional] 
 **search** | **str**| plain search that will match with video titles, reporter names and more | [optional] 
 **state** | [**AbuseStateSet**](.md)|  | [optional] 
 **search_reporter** | **str**| only list reports of a specific reporter | [optional] 
 **search_reportee** | **str**| only list reports of a specific reportee | [optional] 
 **search_video** | **str**| only list reports of a specific video | [optional] 
 **search_video_channel** | **str**| only list reports of a specific video channel | [optional] 
 **video_is** | **str**| only list blacklisted or deleted videos | [optional] 
 **filter** | **str**| only list account, comment or video reports | [optional] 
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort abuses by criteria | [optional] 

### Return type

[**list[Abuse]**](Abuse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **abuses_post**
> abuses_post(inline_object10)

Report an abuse

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.AbusesApi(api_client)
    inline_object10 = peertube.InlineObject10() # InlineObject10 | 

    try:
        # Report an abuse
        api_instance.abuses_post(inline_object10)
    except ApiException as e:
        print("Exception when calling AbusesApi->abuses_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object10** | [**InlineObject10**](InlineObject10.md)|  | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**400** | incorrect request parameters |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_abuses_get**
> list[Abuse] users_me_abuses_get(id=id, state=state, start=start, count=count, sort=sort)

List my abuses

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.AbusesApi(api_client)
    id = 56 # int | only list the report with this id (optional)
state = peertube.AbuseStateSet() # AbuseStateSet |  (optional)
start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = 'sort_example' # str | Sort abuses by criteria (optional)

    try:
        # List my abuses
        api_response = api_instance.users_me_abuses_get(id=id, state=state, start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling AbusesApi->users_me_abuses_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| only list the report with this id | [optional] 
 **state** | [**AbuseStateSet**](.md)|  | [optional] 
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort abuses by criteria | [optional] 

### Return type

[**list[Abuse]**](Abuse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

