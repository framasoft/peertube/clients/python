# LiveVideoResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rtmp_url** | **str** |  | [optional] 
**stream_key** | **str** | RTMP stream key to use to stream into this live video | [optional] 
**save_replay** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


