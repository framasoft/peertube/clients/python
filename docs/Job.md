# Job

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**state** | **str** |  | [optional] 
**type** | **str** |  | [optional] 
**data** | **dict(str, object)** |  | [optional] 
**error** | **dict(str, object)** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**finished_on** | **datetime** |  | [optional] 
**processed_on** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


