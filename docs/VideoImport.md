# VideoImport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**target_url** | **str** |  | [optional] 
**magnet_uri** | **str** |  | [optional] 
**torrent_name** | **str** |  | [optional] 
**state** | [**VideoImportStateConstant**](VideoImportStateConstant.md) |  | [optional] 
**error** | **str** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**updated_at** | **datetime** |  | [optional] 
**video** | [**Video**](Video.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


