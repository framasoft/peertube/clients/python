# Abuse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**reason** | **str** |  | [optional] 
**predefined_reasons** | **list[str]** |  | [optional] 
**reporter_account** | [**Account**](Account.md) |  | [optional] 
**state** | [**AbuseStateConstant**](AbuseStateConstant.md) |  | [optional] 
**moderation_comment** | **str** |  | [optional] 
**video** | [**AbuseVideo**](AbuseVideo.md) |  | [optional] 
**created_at** | **datetime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


