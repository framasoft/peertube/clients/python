# UpdateMe

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**password** | **str** | Your new password | 
**email** | **str** | Your new email | 
**display_nsfw** | **str** | Your new displayNSFW | 
**auto_play_video** | **bool** | Your new autoPlayVideo | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


