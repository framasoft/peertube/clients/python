# VideoDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**uuid** | **str** |  | [optional] 
**is_live** | **bool** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**published_at** | **datetime** |  | [optional] 
**updated_at** | **datetime** |  | [optional] 
**originally_published_at** | **datetime** |  | [optional] 
**category** | [**VideoConstantNumber**](VideoConstantNumber.md) |  | [optional] 
**licence** | [**VideoConstantNumber**](VideoConstantNumber.md) |  | [optional] 
**language** | [**VideoConstantString**](VideoConstantString.md) |  | [optional] 
**privacy** | [**VideoPrivacyConstant**](VideoPrivacyConstant.md) |  | [optional] 
**description** | **str** |  | [optional] 
**duration** | **int** |  | [optional] 
**is_local** | **bool** |  | [optional] 
**name** | **str** |  | [optional] 
**thumbnail_path** | **str** |  | [optional] 
**preview_path** | **str** |  | [optional] 
**embed_path** | **str** |  | [optional] 
**views** | **int** |  | [optional] 
**likes** | **int** |  | [optional] 
**dislikes** | **int** |  | [optional] 
**nsfw** | **bool** |  | [optional] 
**wait_transcoding** | **bool** |  | [optional] 
**state** | [**VideoStateConstant**](VideoStateConstant.md) |  | [optional] 
**scheduled_update** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  | [optional] 
**blacklisted** | **bool** |  | [optional] 
**blacklisted_reason** | **str** |  | [optional] 
**account** | [**Account**](Account.md) |  | [optional] 
**channel** | [**VideoChannel**](VideoChannel.md) |  | [optional] 
**user_history** | [**VideoUserHistory**](VideoUserHistory.md) |  | [optional] 
**description_path** | **str** |  | [optional] 
**support** | **str** | A text tell the audience how to support the video creator | [optional] 
**tags** | **list[str]** |  | [optional] 
**files** | [**list[VideoFile]**](VideoFile.md) | WebTorrent/raw video files. Can be empty if WebTorrent is disabled on the server. In this case, video files will be in the \&quot;streamingPlaylists[].files\&quot; property | [optional] 
**comments_enabled** | **bool** |  | [optional] 
**download_enabled** | **bool** |  | [optional] 
**tracker_urls** | **list[str]** |  | [optional] 
**streaming_playlists** | [**list[VideoStreamingPlaylists]**](VideoStreamingPlaylists.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


