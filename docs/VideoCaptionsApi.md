# peertube.VideoCaptionsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videos_id_captions_caption_language_delete**](VideoCaptionsApi.md#videos_id_captions_caption_language_delete) | **DELETE** /videos/{id}/captions/{captionLanguage} | Delete a video caption
[**videos_id_captions_caption_language_put**](VideoCaptionsApi.md#videos_id_captions_caption_language_put) | **PUT** /videos/{id}/captions/{captionLanguage} | Add or replace a video caption
[**videos_id_captions_get**](VideoCaptionsApi.md#videos_id_captions_get) | **GET** /videos/{id}/captions | List captions of a video


# **videos_id_captions_caption_language_delete**
> videos_id_captions_caption_language_delete(id, caption_language)

Delete a video caption

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoCaptionsApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid
caption_language = 'caption_language_example' # str | The caption language

    try:
        # Delete a video caption
        api_instance.videos_id_captions_caption_language_delete(id, caption_language)
    except ApiException as e:
        print("Exception when calling VideoCaptionsApi->videos_id_captions_caption_language_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **caption_language** | **str**| The caption language | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**404** | video or language or caption for that language not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_captions_caption_language_put**
> videos_id_captions_caption_language_put(id, caption_language, captionfile=captionfile)

Add or replace a video caption

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoCaptionsApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid
caption_language = 'caption_language_example' # str | The caption language
captionfile = '/path/to/file' # file | The file to upload. (optional)

    try:
        # Add or replace a video caption
        api_instance.videos_id_captions_caption_language_put(id, caption_language, captionfile=captionfile)
    except ApiException as e:
        print("Exception when calling VideoCaptionsApi->videos_id_captions_caption_language_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **caption_language** | **str**| The caption language | 
 **captionfile** | **file**| The file to upload. | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**404** | video or language not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_captions_get**
> InlineResponse2002 videos_id_captions_get(id)

List captions of a video

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoCaptionsApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid

    try:
        # List captions of a video
        api_response = api_instance.videos_id_captions_get(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoCaptionsApi->videos_id_captions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

