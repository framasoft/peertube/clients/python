# ServerConfigAutoBlacklistVideos

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**of_users** | [**ServerConfigEmail**](ServerConfigEmail.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


