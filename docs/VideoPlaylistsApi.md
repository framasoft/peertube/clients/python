# peertube.VideoPlaylistsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**users_me_video_playlists_videos_exist_get**](VideoPlaylistsApi.md#users_me_video_playlists_videos_exist_get) | **GET** /users/me/video-playlists/videos-exist | Check video exists in my playlists
[**video_playlists_get**](VideoPlaylistsApi.md#video_playlists_get) | **GET** /video-playlists | List video playlists
[**video_playlists_id_delete**](VideoPlaylistsApi.md#video_playlists_id_delete) | **DELETE** /video-playlists/{id} | Delete a video playlist
[**video_playlists_id_get**](VideoPlaylistsApi.md#video_playlists_id_get) | **GET** /video-playlists/{id} | Get a video playlist
[**video_playlists_id_put**](VideoPlaylistsApi.md#video_playlists_id_put) | **PUT** /video-playlists/{id} | Update a video playlist
[**video_playlists_id_videos_get**](VideoPlaylistsApi.md#video_playlists_id_videos_get) | **GET** /video-playlists/{id}/videos | List videos of a playlist
[**video_playlists_id_videos_playlist_element_id_delete**](VideoPlaylistsApi.md#video_playlists_id_videos_playlist_element_id_delete) | **DELETE** /video-playlists/{id}/videos/{playlistElementId} | Delete an element from a playlist
[**video_playlists_id_videos_playlist_element_id_put**](VideoPlaylistsApi.md#video_playlists_id_videos_playlist_element_id_put) | **PUT** /video-playlists/{id}/videos/{playlistElementId} | Update a playlist element
[**video_playlists_id_videos_post**](VideoPlaylistsApi.md#video_playlists_id_videos_post) | **POST** /video-playlists/{id}/videos | Add a video in a playlist
[**video_playlists_id_videos_reorder_post**](VideoPlaylistsApi.md#video_playlists_id_videos_reorder_post) | **POST** /video-playlists/{id}/videos/reorder | Reorder a playlist
[**video_playlists_post**](VideoPlaylistsApi.md#video_playlists_post) | **POST** /video-playlists | Create a video playlist
[**video_playlists_privacies_get**](VideoPlaylistsApi.md#video_playlists_privacies_get) | **GET** /video-playlists/privacies | List available playlist privacies


# **users_me_video_playlists_videos_exist_get**
> InlineResponse2007 users_me_video_playlists_videos_exist_get(video_ids)

Check video exists in my playlists

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoPlaylistsApi(api_client)
    video_ids = [56] # list[int] | The video ids to check

    try:
        # Check video exists in my playlists
        api_response = api_instance.users_me_video_playlists_videos_exist_get(video_ids)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoPlaylistsApi->users_me_video_playlists_videos_exist_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **video_ids** | [**list[int]**](int.md)| The video ids to check | 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_playlists_get**
> InlineResponse2004 video_playlists_get(start=start, count=count, sort=sort)

List video playlists

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoPlaylistsApi(api_client)
    start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = '-createdAt' # str | Sort column (optional)

    try:
        # List video playlists
        api_response = api_instance.video_playlists_get(start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoPlaylistsApi->video_playlists_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort column | [optional] 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_playlists_id_delete**
> video_playlists_id_delete(id)

Delete a video playlist

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoPlaylistsApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid

    try:
        # Delete a video playlist
        api_instance.video_playlists_id_delete(id)
    except ApiException as e:
        print("Exception when calling VideoPlaylistsApi->video_playlists_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_playlists_id_get**
> VideoPlaylist video_playlists_id_get(id)

Get a video playlist

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoPlaylistsApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid

    try:
        # Get a video playlist
        api_response = api_instance.video_playlists_id_get(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoPlaylistsApi->video_playlists_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

[**VideoPlaylist**](VideoPlaylist.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_playlists_id_put**
> video_playlists_id_put(id, display_name=display_name, thumbnailfile=thumbnailfile, privacy=privacy, description=description, video_channel_id=video_channel_id)

Update a video playlist

If the video playlist is set as public, the playlist must have a assigned channel.

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoPlaylistsApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid
display_name = 'display_name_example' # str | Video playlist display name (optional)
thumbnailfile = '/path/to/file' # file | Video playlist thumbnail file (optional)
privacy = peertube.VideoPlaylistPrivacySet() # VideoPlaylistPrivacySet |  (optional)
description = 'description_example' # str | Video playlist description (optional)
video_channel_id = 56 # int | Video channel in which the playlist will be published (optional)

    try:
        # Update a video playlist
        api_instance.video_playlists_id_put(id, display_name=display_name, thumbnailfile=thumbnailfile, privacy=privacy, description=description, video_channel_id=video_channel_id)
    except ApiException as e:
        print("Exception when calling VideoPlaylistsApi->video_playlists_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **display_name** | **str**| Video playlist display name | [optional] 
 **thumbnailfile** | **file**| Video playlist thumbnail file | [optional] 
 **privacy** | [**VideoPlaylistPrivacySet**](VideoPlaylistPrivacySet.md)|  | [optional] 
 **description** | **str**| Video playlist description | [optional] 
 **video_channel_id** | **int**| Video channel in which the playlist will be published | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_playlists_id_videos_get**
> VideoListResponse video_playlists_id_videos_get(id)

List videos of a playlist

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoPlaylistsApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid

    try:
        # List videos of a playlist
        api_response = api_instance.video_playlists_id_videos_get(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoPlaylistsApi->video_playlists_id_videos_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_playlists_id_videos_playlist_element_id_delete**
> video_playlists_id_videos_playlist_element_id_delete(id, playlist_element_id)

Delete an element from a playlist

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoPlaylistsApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid
playlist_element_id = 56 # int | Playlist element id

    try:
        # Delete an element from a playlist
        api_instance.video_playlists_id_videos_playlist_element_id_delete(id, playlist_element_id)
    except ApiException as e:
        print("Exception when calling VideoPlaylistsApi->video_playlists_id_videos_playlist_element_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **playlist_element_id** | **int**| Playlist element id | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_playlists_id_videos_playlist_element_id_put**
> video_playlists_id_videos_playlist_element_id_put(id, playlist_element_id, inline_object18=inline_object18)

Update a playlist element

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoPlaylistsApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid
playlist_element_id = 56 # int | Playlist element id
inline_object18 = peertube.InlineObject18() # InlineObject18 |  (optional)

    try:
        # Update a playlist element
        api_instance.video_playlists_id_videos_playlist_element_id_put(id, playlist_element_id, inline_object18=inline_object18)
    except ApiException as e:
        print("Exception when calling VideoPlaylistsApi->video_playlists_id_videos_playlist_element_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **playlist_element_id** | **int**| Playlist element id | 
 **inline_object18** | [**InlineObject18**](InlineObject18.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_playlists_id_videos_post**
> InlineResponse2006 video_playlists_id_videos_post(id, inline_object16=inline_object16)

Add a video in a playlist

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoPlaylistsApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid
inline_object16 = peertube.InlineObject16() # InlineObject16 |  (optional)

    try:
        # Add a video in a playlist
        api_response = api_instance.video_playlists_id_videos_post(id, inline_object16=inline_object16)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoPlaylistsApi->video_playlists_id_videos_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **inline_object16** | [**InlineObject16**](InlineObject16.md)|  | [optional] 

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_playlists_id_videos_reorder_post**
> video_playlists_id_videos_reorder_post(id, inline_object17=inline_object17)

Reorder a playlist

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoPlaylistsApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid
inline_object17 = peertube.InlineObject17() # InlineObject17 |  (optional)

    try:
        # Reorder a playlist
        api_instance.video_playlists_id_videos_reorder_post(id, inline_object17=inline_object17)
    except ApiException as e:
        print("Exception when calling VideoPlaylistsApi->video_playlists_id_videos_reorder_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **inline_object17** | [**InlineObject17**](InlineObject17.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_playlists_post**
> InlineResponse2005 video_playlists_post(display_name, thumbnailfile=thumbnailfile, privacy=privacy, description=description, video_channel_id=video_channel_id)

Create a video playlist

If the video playlist is set as public, the videoChannelId is mandatory.

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoPlaylistsApi(api_client)
    display_name = 'display_name_example' # str | Video playlist display name
thumbnailfile = '/path/to/file' # file | Video playlist thumbnail file (optional)
privacy = peertube.VideoPlaylistPrivacySet() # VideoPlaylistPrivacySet |  (optional)
description = 'description_example' # str | Video playlist description (optional)
video_channel_id = 56 # int | Video channel in which the playlist will be published (optional)

    try:
        # Create a video playlist
        api_response = api_instance.video_playlists_post(display_name, thumbnailfile=thumbnailfile, privacy=privacy, description=description, video_channel_id=video_channel_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoPlaylistsApi->video_playlists_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **display_name** | **str**| Video playlist display name | 
 **thumbnailfile** | **file**| Video playlist thumbnail file | [optional] 
 **privacy** | [**VideoPlaylistPrivacySet**](VideoPlaylistPrivacySet.md)|  | [optional] 
 **description** | **str**| Video playlist description | [optional] 
 **video_channel_id** | **int**| Video channel in which the playlist will be published | [optional] 

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_playlists_privacies_get**
> list[str] video_playlists_privacies_get()

List available playlist privacies

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoPlaylistsApi(api_client)
    
    try:
        # List available playlist privacies
        api_response = api_instance.video_playlists_privacies_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoPlaylistsApi->video_playlists_privacies_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**list[str]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

