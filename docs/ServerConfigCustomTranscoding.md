# ServerConfigCustomTranscoding

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **bool** |  | [optional] 
**allow_additional_extensions** | **bool** |  | [optional] 
**allow_audio_files** | **bool** |  | [optional] 
**threads** | **int** |  | [optional] 
**resolutions** | [**ServerConfigCustomTranscodingResolutions**](ServerConfigCustomTranscodingResolutions.md) |  | [optional] 
**hls** | [**ServerConfigEmail**](ServerConfigEmail.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


