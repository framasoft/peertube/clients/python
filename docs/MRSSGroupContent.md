# MRSSGroupContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **str** |  | [optional] 
**file_size** | **int** |  | [optional] 
**type** | **str** |  | [optional] 
**framerate** | **int** |  | [optional] 
**duration** | **int** |  | [optional] 
**height** | **int** |  | [optional] 
**lang** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


