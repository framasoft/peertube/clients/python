# peertube.MySubscriptionsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**users_me_subscriptions_exist_get**](MySubscriptionsApi.md#users_me_subscriptions_exist_get) | **GET** /users/me/subscriptions/exist | Get if subscriptions exist for my user
[**users_me_subscriptions_get**](MySubscriptionsApi.md#users_me_subscriptions_get) | **GET** /users/me/subscriptions | Get my user subscriptions
[**users_me_subscriptions_post**](MySubscriptionsApi.md#users_me_subscriptions_post) | **POST** /users/me/subscriptions | Add subscription to my user
[**users_me_subscriptions_subscription_handle_delete**](MySubscriptionsApi.md#users_me_subscriptions_subscription_handle_delete) | **DELETE** /users/me/subscriptions/{subscriptionHandle} | Delete subscription of my user
[**users_me_subscriptions_subscription_handle_get**](MySubscriptionsApi.md#users_me_subscriptions_subscription_handle_get) | **GET** /users/me/subscriptions/{subscriptionHandle} | Get subscription of my user
[**users_me_subscriptions_videos_get**](MySubscriptionsApi.md#users_me_subscriptions_videos_get) | **GET** /users/me/subscriptions/videos | List videos of subscriptions of my user


# **users_me_subscriptions_exist_get**
> object users_me_subscriptions_exist_get(uris)

Get if subscriptions exist for my user

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MySubscriptionsApi(api_client)
    uris = ['uris_example'] # list[str] | list of uris to check if each is part of the user subscriptions

    try:
        # Get if subscriptions exist for my user
        api_response = api_instance.users_me_subscriptions_exist_get(uris)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling MySubscriptionsApi->users_me_subscriptions_exist_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uris** | [**list[str]**](str.md)| list of uris to check if each is part of the user subscriptions | 

### Return type

**object**

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_subscriptions_get**
> users_me_subscriptions_get(start=start, count=count, sort=sort)

Get my user subscriptions

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MySubscriptionsApi(api_client)
    start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = '-createdAt' # str | Sort column (optional)

    try:
        # Get my user subscriptions
        api_instance.users_me_subscriptions_get(start=start, count=count, sort=sort)
    except ApiException as e:
        print("Exception when calling MySubscriptionsApi->users_me_subscriptions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort column | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_subscriptions_post**
> users_me_subscriptions_post(inline_object1=inline_object1)

Add subscription to my user

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MySubscriptionsApi(api_client)
    inline_object1 = peertube.InlineObject1() # InlineObject1 |  (optional)

    try:
        # Add subscription to my user
        api_instance.users_me_subscriptions_post(inline_object1=inline_object1)
    except ApiException as e:
        print("Exception when calling MySubscriptionsApi->users_me_subscriptions_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object1** | [**InlineObject1**](InlineObject1.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_subscriptions_subscription_handle_delete**
> users_me_subscriptions_subscription_handle_delete(subscription_handle)

Delete subscription of my user

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MySubscriptionsApi(api_client)
    subscription_handle = 'my_username | my_username@example.com' # str | The subscription handle

    try:
        # Delete subscription of my user
        api_instance.users_me_subscriptions_subscription_handle_delete(subscription_handle)
    except ApiException as e:
        print("Exception when calling MySubscriptionsApi->users_me_subscriptions_subscription_handle_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscription_handle** | **str**| The subscription handle | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_subscriptions_subscription_handle_get**
> VideoChannel users_me_subscriptions_subscription_handle_get(subscription_handle)

Get subscription of my user

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MySubscriptionsApi(api_client)
    subscription_handle = 'my_username | my_username@example.com' # str | The subscription handle

    try:
        # Get subscription of my user
        api_response = api_instance.users_me_subscriptions_subscription_handle_get(subscription_handle)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling MySubscriptionsApi->users_me_subscriptions_subscription_handle_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscription_handle** | **str**| The subscription handle | 

### Return type

[**VideoChannel**](VideoChannel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **users_me_subscriptions_videos_get**
> VideoListResponse users_me_subscriptions_videos_get(category_one_of=category_one_of, tags_one_of=tags_one_of, tags_all_of=tags_all_of, licence_one_of=licence_one_of, language_one_of=language_one_of, nsfw=nsfw, filter=filter, skip_count=skip_count, start=start, count=count, sort=sort)

List videos of subscriptions of my user

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.MySubscriptionsApi(api_client)
    category_one_of = peertube.OneOfintegerarray() # OneOfintegerarray | category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) (optional)
tags_one_of = peertube.OneOfstringarray() # OneOfstringarray | tag(s) of the video (optional)
tags_all_of = peertube.OneOfstringarray() # OneOfstringarray | tag(s) of the video, where all should be present in the video (optional)
licence_one_of = peertube.OneOfintegerarray() # OneOfintegerarray | licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) (optional)
language_one_of = peertube.OneOfstringarray() # OneOfstringarray | language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use `_unknown` to filter on videos that don't have a video language (optional)
nsfw = 'nsfw_example' # str | whether to include nsfw videos, if any (optional)
filter = 'filter_example' # str | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
skip_count = 'false' # str | if you don't need the `total` in the response (optional) (default to 'false')
start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = 'sort_example' # str | Sort videos by criteria (optional)

    try:
        # List videos of subscriptions of my user
        api_response = api_instance.users_me_subscriptions_videos_get(category_one_of=category_one_of, tags_one_of=tags_one_of, tags_all_of=tags_all_of, licence_one_of=licence_one_of, language_one_of=language_one_of, nsfw=nsfw, filter=filter, skip_count=skip_count, start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling MySubscriptionsApi->users_me_subscriptions_videos_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_one_of** | [**OneOfintegerarray**](.md)| category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) | [optional] 
 **tags_one_of** | [**OneOfstringarray**](.md)| tag(s) of the video | [optional] 
 **tags_all_of** | [**OneOfstringarray**](.md)| tag(s) of the video, where all should be present in the video | [optional] 
 **licence_one_of** | [**OneOfintegerarray**](.md)| licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) | [optional] 
 **language_one_of** | [**OneOfstringarray**](.md)| language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional] 
 **nsfw** | **str**| whether to include nsfw videos, if any | [optional] 
 **filter** | **str**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] 
 **skip_count** | **str**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to &#39;false&#39;]
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort videos by criteria | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

