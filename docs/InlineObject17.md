# InlineObject17

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_position** | **int** | Start position of the element to reorder | 
**insert_after_position** | **int** | New position for the block to reorder, to add the block before the first element | 
**reorder_length** | **int** | How many element from &#x60;startPosition&#x60; to reorder | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


