# VideoPrivacySet

The video privacy (Public = `1`, Unlisted = `2`, Private = `3`, Internal = `4`)
## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


