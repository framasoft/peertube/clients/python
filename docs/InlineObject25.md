# InlineObject25

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**npm_name** | **str** | name of the plugin/theme in its package.json | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


