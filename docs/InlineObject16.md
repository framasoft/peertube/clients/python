# InlineObject16

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**video_id** | **int** | Video to add in the playlist | 
**start_timestamp** | **int** | Start the video at this specific timestamp (in seconds) | [optional] 
**stop_timestamp** | **int** | Stop the video at this specific timestamp (in seconds) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


