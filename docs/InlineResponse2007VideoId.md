# InlineResponse2007VideoId

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**playlist_element_id** | **int** |  | [optional] 
**playlist_id** | **int** |  | [optional] 
**start_timestamp** | **int** |  | [optional] 
**stop_timestamp** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


