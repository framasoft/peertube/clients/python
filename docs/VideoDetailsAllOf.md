# VideoDetailsAllOf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description_path** | **str** |  | [optional] 
**support** | **str** | A text tell the audience how to support the video creator | [optional] 
**channel** | [**VideoChannel**](VideoChannel.md) |  | [optional] 
**account** | [**Account**](Account.md) |  | [optional] 
**tags** | **list[str]** |  | [optional] 
**files** | [**list[VideoFile]**](VideoFile.md) | WebTorrent/raw video files. Can be empty if WebTorrent is disabled on the server. In this case, video files will be in the \&quot;streamingPlaylists[].files\&quot; property | [optional] 
**comments_enabled** | **bool** |  | [optional] 
**download_enabled** | **bool** |  | [optional] 
**tracker_urls** | **list[str]** |  | [optional] 
**streaming_playlists** | [**list[VideoStreamingPlaylists]**](VideoStreamingPlaylists.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


