# NotificationVideoImport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**video** | [**VideoInfo**](VideoInfo.md) |  | [optional] 
**torrent_name** | **str** |  | [optional] 
**magnet_uri** | **str** |  | [optional] 
**target_uri** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


