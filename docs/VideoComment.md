# VideoComment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**url** | **str** |  | [optional] 
**text** | **str** |  | [optional] 
**thread_id** | **int** |  | [optional] 
**in_reply_to_comment_id** | **int** |  | [optional] 
**video_id** | **int** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**updated_at** | **datetime** |  | [optional] 
**total_replies_from_video_author** | **int** |  | [optional] 
**total_replies** | **int** |  | [optional] 
**account** | [**Account**](Account.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


