# VideoStreamingPlaylists

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**type** | **int** | Playlist type (HLS &#x3D; &#x60;1&#x60;) | [optional] 
**playlist_url** | **str** |  | [optional] 
**segments_sha256_url** | **str** |  | [optional] 
**files** | [**list[VideoFile]**](VideoFile.md) | Video files associated to this playlist. The difference with the root \&quot;files\&quot; property is that these files are fragmented, so they can be used in this streaming playlist (HLS etc) | [optional] 
**redundancies** | [**list[VideoStreamingPlaylistsRedundancies]**](VideoStreamingPlaylistsRedundancies.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


