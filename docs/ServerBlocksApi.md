# peertube.ServerBlocksApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blocklist_servers_get**](ServerBlocksApi.md#blocklist_servers_get) | **GET** /blocklist/servers | List server blocks
[**blocklist_servers_host_delete**](ServerBlocksApi.md#blocklist_servers_host_delete) | **DELETE** /blocklist/servers/{host} | Unblock a server by its domain
[**blocklist_servers_post**](ServerBlocksApi.md#blocklist_servers_post) | **POST** /blocklist/servers | Block a server


# **blocklist_servers_get**
> blocklist_servers_get(start=start, count=count, sort=sort)

List server blocks

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.ServerBlocksApi(api_client)
    start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = '-createdAt' # str | Sort column (optional)

    try:
        # List server blocks
        api_instance.blocklist_servers_get(start=start, count=count, sort=sort)
    except ApiException as e:
        print("Exception when calling ServerBlocksApi->blocklist_servers_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort column | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **blocklist_servers_host_delete**
> blocklist_servers_host_delete(host)

Unblock a server by its domain

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.ServerBlocksApi(api_client)
    host = 'host_example' # str | server domain to unblock

    try:
        # Unblock a server by its domain
        api_instance.blocklist_servers_host_delete(host)
    except ApiException as e:
        print("Exception when calling ServerBlocksApi->blocklist_servers_host_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host** | **str**| server domain to unblock | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**404** | account block does not exist |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **blocklist_servers_post**
> blocklist_servers_post(inline_object22=inline_object22)

Block a server

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.ServerBlocksApi(api_client)
    inline_object22 = peertube.InlineObject22() # InlineObject22 |  (optional)

    try:
        # Block a server
        api_instance.blocklist_servers_post(inline_object22=inline_object22)
    except ApiException as e:
        print("Exception when calling ServerBlocksApi->blocklist_servers_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object22** | [**InlineObject22**](InlineObject22.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**409** | self-blocking forbidden |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

