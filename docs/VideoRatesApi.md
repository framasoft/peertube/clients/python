# peertube.VideoRatesApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**users_me_videos_video_id_rating_get**](VideoRatesApi.md#users_me_videos_video_id_rating_get) | **GET** /users/me/videos/{videoId}/rating | Get rate of my user for a video
[**videos_id_rate_put**](VideoRatesApi.md#videos_id_rate_put) | **PUT** /videos/{id}/rate | Like/dislike a video


# **users_me_videos_video_id_rating_get**
> GetMeVideoRating users_me_videos_video_id_rating_get(video_id)

Get rate of my user for a video

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoRatesApi(api_client)
    video_id = 'video_id_example' # str | The video id 

    try:
        # Get rate of my user for a video
        api_response = api_instance.users_me_videos_video_id_rating_get(video_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoRatesApi->users_me_videos_video_id_rating_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **video_id** | **str**| The video id  | 

### Return type

[**GetMeVideoRating**](GetMeVideoRating.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_rate_put**
> videos_id_rate_put(id)

Like/dislike a video

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoRatesApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid

    try:
        # Like/dislike a video
        api_instance.videos_id_rate_put(id)
    except ApiException as e:
        print("Exception when calling VideoRatesApi->videos_id_rate_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**404** | video does not exist |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

