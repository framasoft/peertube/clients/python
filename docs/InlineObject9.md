# InlineObject9

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_id** | **int** | Channel id that will contain this live video | 
**save_replay** | **bool** |  | [optional] 
**thumbnailfile** | **file** | Live video/replay thumbnail file | [optional] 
**previewfile** | **file** | Live video/replay preview file | [optional] 
**privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | [optional] 
**category** | **str** | Live video/replay category | [optional] 
**licence** | **str** | Live video/replay licence | [optional] 
**language** | **str** | Live video/replay language | [optional] 
**description** | **str** | Live video/replay description | [optional] 
**support** | **str** | A text tell the audience how to support the creator | [optional] 
**nsfw** | **bool** | Whether or not this live video/replay contains sensitive content | [optional] 
**name** | **str** | Live video/replay name | 
**tags** | **list[str]** | Live video/replay tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
**comments_enabled** | **bool** | Enable or disable comments for this live video/replay | [optional] 
**download_enabled** | **bool** | Enable or disable downloading for the replay of this live | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


