# peertube.VideoApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**accounts_name_videos_get**](VideoApi.md#accounts_name_videos_get) | **GET** /accounts/{name}/videos | List videos of an account
[**video_channels_channel_handle_videos_get**](VideoApi.md#video_channels_channel_handle_videos_get) | **GET** /video-channels/{channelHandle}/videos | List videos of a video channel
[**videos_categories_get**](VideoApi.md#videos_categories_get) | **GET** /videos/categories | List available video categories
[**videos_get**](VideoApi.md#videos_get) | **GET** /videos | List videos
[**videos_id_delete**](VideoApi.md#videos_id_delete) | **DELETE** /videos/{id} | Delete a video
[**videos_id_description_get**](VideoApi.md#videos_id_description_get) | **GET** /videos/{id}/description | Get complete video description
[**videos_id_get**](VideoApi.md#videos_id_get) | **GET** /videos/{id} | Get a video
[**videos_id_put**](VideoApi.md#videos_id_put) | **PUT** /videos/{id} | Update a video
[**videos_id_views_post**](VideoApi.md#videos_id_views_post) | **POST** /videos/{id}/views | Add a view to a video
[**videos_id_watching_put**](VideoApi.md#videos_id_watching_put) | **PUT** /videos/{id}/watching | Set watching progress of a video
[**videos_imports_post**](VideoApi.md#videos_imports_post) | **POST** /videos/imports | Import a video
[**videos_languages_get**](VideoApi.md#videos_languages_get) | **GET** /videos/languages | List available video languages
[**videos_licences_get**](VideoApi.md#videos_licences_get) | **GET** /videos/licences | List available video licences
[**videos_live_id_get**](VideoApi.md#videos_live_id_get) | **GET** /videos/live/{id} | Get a live information
[**videos_live_id_put**](VideoApi.md#videos_live_id_put) | **PUT** /videos/live/{id} | Update a live information
[**videos_live_post**](VideoApi.md#videos_live_post) | **POST** /videos/live | Create a live
[**videos_privacies_get**](VideoApi.md#videos_privacies_get) | **GET** /videos/privacies | List available video privacies
[**videos_upload_post**](VideoApi.md#videos_upload_post) | **POST** /videos/upload | Upload a video


# **accounts_name_videos_get**
> VideoListResponse accounts_name_videos_get(name, category_one_of=category_one_of, tags_one_of=tags_one_of, tags_all_of=tags_all_of, licence_one_of=licence_one_of, language_one_of=language_one_of, nsfw=nsfw, filter=filter, skip_count=skip_count, start=start, count=count, sort=sort)

List videos of an account

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    name = 'chocobozzz | chocobozzz@example.org' # str | The name of the account
category_one_of = peertube.OneOfintegerarray() # OneOfintegerarray | category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) (optional)
tags_one_of = peertube.OneOfstringarray() # OneOfstringarray | tag(s) of the video (optional)
tags_all_of = peertube.OneOfstringarray() # OneOfstringarray | tag(s) of the video, where all should be present in the video (optional)
licence_one_of = peertube.OneOfintegerarray() # OneOfintegerarray | licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) (optional)
language_one_of = peertube.OneOfstringarray() # OneOfstringarray | language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use `_unknown` to filter on videos that don't have a video language (optional)
nsfw = 'nsfw_example' # str | whether to include nsfw videos, if any (optional)
filter = 'filter_example' # str | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
skip_count = 'false' # str | if you don't need the `total` in the response (optional) (default to 'false')
start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = 'sort_example' # str | Sort videos by criteria (optional)

    try:
        # List videos of an account
        api_response = api_instance.accounts_name_videos_get(name, category_one_of=category_one_of, tags_one_of=tags_one_of, tags_all_of=tags_all_of, licence_one_of=licence_one_of, language_one_of=language_one_of, nsfw=nsfw, filter=filter, skip_count=skip_count, start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->accounts_name_videos_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| The name of the account | 
 **category_one_of** | [**OneOfintegerarray**](.md)| category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) | [optional] 
 **tags_one_of** | [**OneOfstringarray**](.md)| tag(s) of the video | [optional] 
 **tags_all_of** | [**OneOfstringarray**](.md)| tag(s) of the video, where all should be present in the video | [optional] 
 **licence_one_of** | [**OneOfintegerarray**](.md)| licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) | [optional] 
 **language_one_of** | [**OneOfstringarray**](.md)| language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional] 
 **nsfw** | **str**| whether to include nsfw videos, if any | [optional] 
 **filter** | **str**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] 
 **skip_count** | **str**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to &#39;false&#39;]
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort videos by criteria | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **video_channels_channel_handle_videos_get**
> VideoListResponse video_channels_channel_handle_videos_get(channel_handle, category_one_of=category_one_of, tags_one_of=tags_one_of, tags_all_of=tags_all_of, licence_one_of=licence_one_of, language_one_of=language_one_of, nsfw=nsfw, filter=filter, skip_count=skip_count, start=start, count=count, sort=sort)

List videos of a video channel

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    channel_handle = 'my_username | my_username@example.com' # str | The video channel handle
category_one_of = peertube.OneOfintegerarray() # OneOfintegerarray | category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) (optional)
tags_one_of = peertube.OneOfstringarray() # OneOfstringarray | tag(s) of the video (optional)
tags_all_of = peertube.OneOfstringarray() # OneOfstringarray | tag(s) of the video, where all should be present in the video (optional)
licence_one_of = peertube.OneOfintegerarray() # OneOfintegerarray | licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) (optional)
language_one_of = peertube.OneOfstringarray() # OneOfstringarray | language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use `_unknown` to filter on videos that don't have a video language (optional)
nsfw = 'nsfw_example' # str | whether to include nsfw videos, if any (optional)
filter = 'filter_example' # str | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
skip_count = 'false' # str | if you don't need the `total` in the response (optional) (default to 'false')
start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = 'sort_example' # str | Sort videos by criteria (optional)

    try:
        # List videos of a video channel
        api_response = api_instance.video_channels_channel_handle_videos_get(channel_handle, category_one_of=category_one_of, tags_one_of=tags_one_of, tags_all_of=tags_all_of, licence_one_of=licence_one_of, language_one_of=language_one_of, nsfw=nsfw, filter=filter, skip_count=skip_count, start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->video_channels_channel_handle_videos_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_handle** | **str**| The video channel handle | 
 **category_one_of** | [**OneOfintegerarray**](.md)| category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) | [optional] 
 **tags_one_of** | [**OneOfstringarray**](.md)| tag(s) of the video | [optional] 
 **tags_all_of** | [**OneOfstringarray**](.md)| tag(s) of the video, where all should be present in the video | [optional] 
 **licence_one_of** | [**OneOfintegerarray**](.md)| licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) | [optional] 
 **language_one_of** | [**OneOfstringarray**](.md)| language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional] 
 **nsfw** | **str**| whether to include nsfw videos, if any | [optional] 
 **filter** | **str**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] 
 **skip_count** | **str**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to &#39;false&#39;]
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort videos by criteria | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_categories_get**
> list[str] videos_categories_get()

List available video categories

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    
    try:
        # List available video categories
        api_response = api_instance.videos_categories_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_categories_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**list[str]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_get**
> VideoListResponse videos_get(category_one_of=category_one_of, tags_one_of=tags_one_of, tags_all_of=tags_all_of, licence_one_of=licence_one_of, language_one_of=language_one_of, nsfw=nsfw, filter=filter, skip_count=skip_count, start=start, count=count, sort=sort)

List videos

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    category_one_of = peertube.OneOfintegerarray() # OneOfintegerarray | category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) (optional)
tags_one_of = peertube.OneOfstringarray() # OneOfstringarray | tag(s) of the video (optional)
tags_all_of = peertube.OneOfstringarray() # OneOfstringarray | tag(s) of the video, where all should be present in the video (optional)
licence_one_of = peertube.OneOfintegerarray() # OneOfintegerarray | licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) (optional)
language_one_of = peertube.OneOfstringarray() # OneOfstringarray | language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use `_unknown` to filter on videos that don't have a video language (optional)
nsfw = 'nsfw_example' # str | whether to include nsfw videos, if any (optional)
filter = 'filter_example' # str | Special filters which might require special rights:  * `local` - only videos local to the instance  * `all-local` - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * `all` - all videos, showing private and unlisted videos (requires Admin privileges)  (optional)
skip_count = 'false' # str | if you don't need the `total` in the response (optional) (default to 'false')
start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = 'sort_example' # str | Sort videos by criteria (optional)

    try:
        # List videos
        api_response = api_instance.videos_get(category_one_of=category_one_of, tags_one_of=tags_one_of, tags_all_of=tags_all_of, licence_one_of=licence_one_of, language_one_of=language_one_of, nsfw=nsfw, filter=filter, skip_count=skip_count, start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_one_of** | [**OneOfintegerarray**](.md)| category id of the video (see [/videos/categories](#tag/Video/paths/~1videos~1categories/get)) | [optional] 
 **tags_one_of** | [**OneOfstringarray**](.md)| tag(s) of the video | [optional] 
 **tags_all_of** | [**OneOfstringarray**](.md)| tag(s) of the video, where all should be present in the video | [optional] 
 **licence_one_of** | [**OneOfintegerarray**](.md)| licence id of the video (see [/videos/licences](#tag/Video/paths/~1videos~1licences/get)) | [optional] 
 **language_one_of** | [**OneOfstringarray**](.md)| language id of the video (see [/videos/languages](#tag/Video/paths/~1videos~1languages/get)). Use &#x60;_unknown&#x60; to filter on videos that don&#39;t have a video language | [optional] 
 **nsfw** | **str**| whether to include nsfw videos, if any | [optional] 
 **filter** | **str**| Special filters which might require special rights:  * &#x60;local&#x60; - only videos local to the instance  * &#x60;all-local&#x60; - only videos local to the instance, but showing private and unlisted videos (requires Admin privileges)  * &#x60;all&#x60; - all videos, showing private and unlisted videos (requires Admin privileges)  | [optional] 
 **skip_count** | **str**| if you don&#39;t need the &#x60;total&#x60; in the response | [optional] [default to &#39;false&#39;]
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort videos by criteria | [optional] 

### Return type

[**VideoListResponse**](VideoListResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_delete**
> videos_id_delete(id)

Delete a video

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid

    try:
        # Delete a video
        api_instance.videos_id_delete(id)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_description_get**
> str videos_id_description_get(id)

Get complete video description

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid

    try:
        # Get complete video description
        api_response = api_instance.videos_id_description_get(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_id_description_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_get**
> VideoDetails videos_id_get(id)

Get a video

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid

    try:
        # Get a video
        api_response = api_instance.videos_id_get(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

[**VideoDetails**](VideoDetails.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_put**
> videos_id_put(id, thumbnailfile=thumbnailfile, previewfile=previewfile, category=category, licence=licence, language=language, privacy=privacy, description=description, wait_transcoding=wait_transcoding, support=support, nsfw=nsfw, name=name, tags=tags, comments_enabled=comments_enabled, originally_published_at=originally_published_at, schedule_update=schedule_update)

Update a video

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid
thumbnailfile = '/path/to/file' # file | Video thumbnail file (optional)
previewfile = '/path/to/file' # file | Video preview file (optional)
category = 56 # int | Video category (optional)
licence = 56 # int | Video licence (optional)
language = 'language_example' # str | Video language (optional)
privacy = peertube.VideoPrivacySet() # VideoPrivacySet |  (optional)
description = 'description_example' # str | Video description (optional)
wait_transcoding = 'wait_transcoding_example' # str | Whether or not we wait transcoding before publish the video (optional)
support = 'support_example' # str | A text tell the audience how to support the video creator (optional)
nsfw = True # bool | Whether or not this video contains sensitive content (optional)
name = 'name_example' # str | Video name (optional)
tags = 'tags_example' # list[str] | Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
comments_enabled = True # bool | Enable or disable comments for this video (optional)
originally_published_at = '2013-10-20T19:20:30+01:00' # datetime | Date when the content was originally published (optional)
schedule_update = peertube.VideoScheduledUpdate() # VideoScheduledUpdate |  (optional)

    try:
        # Update a video
        api_instance.videos_id_put(id, thumbnailfile=thumbnailfile, previewfile=previewfile, category=category, licence=licence, language=language, privacy=privacy, description=description, wait_transcoding=wait_transcoding, support=support, nsfw=nsfw, name=name, tags=tags, comments_enabled=comments_enabled, originally_published_at=originally_published_at, schedule_update=schedule_update)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **thumbnailfile** | **file**| Video thumbnail file | [optional] 
 **previewfile** | **file**| Video preview file | [optional] 
 **category** | **int**| Video category | [optional] 
 **licence** | **int**| Video licence | [optional] 
 **language** | **str**| Video language | [optional] 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] 
 **description** | **str**| Video description | [optional] 
 **wait_transcoding** | **str**| Whether or not we wait transcoding before publish the video | [optional] 
 **support** | **str**| A text tell the audience how to support the video creator | [optional] 
 **nsfw** | **bool**| Whether or not this video contains sensitive content | [optional] 
 **name** | **str**| Video name | [optional] 
 **tags** | [**list[str]**](str.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
 **comments_enabled** | **bool**| Enable or disable comments for this video | [optional] 
 **originally_published_at** | **datetime**| Date when the content was originally published | [optional] 
 **schedule_update** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_views_post**
> videos_id_views_post(id)

Add a view to a video

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid

    try:
        # Add a view to a video
        api_instance.videos_id_views_post(id)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_id_views_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_watching_put**
> videos_id_watching_put(id, user_watching_video)

Set watching progress of a video

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid
user_watching_video = peertube.UserWatchingVideo() # UserWatchingVideo | 

    try:
        # Set watching progress of a video
        api_instance.videos_id_watching_put(id, user_watching_video)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_id_watching_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **user_watching_video** | [**UserWatchingVideo**](UserWatchingVideo.md)|  | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_imports_post**
> VideoUploadResponse videos_imports_post(channel_id, name, torrentfile=torrentfile, target_url=target_url, magnet_uri=magnet_uri, thumbnailfile=thumbnailfile, previewfile=previewfile, privacy=privacy, category=category, licence=licence, language=language, description=description, wait_transcoding=wait_transcoding, support=support, nsfw=nsfw, tags=tags, comments_enabled=comments_enabled, download_enabled=download_enabled, schedule_update=schedule_update)

Import a video

Import a torrent or magnetURI or HTTP resource (if enabled by the instance administrator)

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    channel_id = 56 # int | Channel id that will contain this video
name = 'name_example' # str | Video name
torrentfile = '/path/to/file' # file | Torrent File (optional)
target_url = 'target_url_example' # str | HTTP target URL (optional)
magnet_uri = 'magnet_uri_example' # str | Magnet URI (optional)
thumbnailfile = '/path/to/file' # file | Video thumbnail file (optional)
previewfile = '/path/to/file' # file | Video preview file (optional)
privacy = peertube.VideoPrivacySet() # VideoPrivacySet |  (optional)
category = 'category_example' # str | Video category (optional)
licence = 'licence_example' # str | Video licence (optional)
language = 'language_example' # str | Video language (optional)
description = 'description_example' # str | Video description (optional)
wait_transcoding = True # bool | Whether or not we wait transcoding before publish the video (optional)
support = 'support_example' # str | A text tell the audience how to support the video creator (optional)
nsfw = True # bool | Whether or not this video contains sensitive content (optional)
tags = 'tags_example' # list[str] | Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
comments_enabled = True # bool | Enable or disable comments for this video (optional)
download_enabled = True # bool | Enable or disable downloading for this video (optional)
schedule_update = peertube.VideoScheduledUpdate() # VideoScheduledUpdate |  (optional)

    try:
        # Import a video
        api_response = api_instance.videos_imports_post(channel_id, name, torrentfile=torrentfile, target_url=target_url, magnet_uri=magnet_uri, thumbnailfile=thumbnailfile, previewfile=previewfile, privacy=privacy, category=category, licence=licence, language=language, description=description, wait_transcoding=wait_transcoding, support=support, nsfw=nsfw, tags=tags, comments_enabled=comments_enabled, download_enabled=download_enabled, schedule_update=schedule_update)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_imports_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_id** | **int**| Channel id that will contain this video | 
 **name** | **str**| Video name | 
 **torrentfile** | **file**| Torrent File | [optional] 
 **target_url** | **str**| HTTP target URL | [optional] 
 **magnet_uri** | **str**| Magnet URI | [optional] 
 **thumbnailfile** | **file**| Video thumbnail file | [optional] 
 **previewfile** | **file**| Video preview file | [optional] 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] 
 **category** | **str**| Video category | [optional] 
 **licence** | **str**| Video licence | [optional] 
 **language** | **str**| Video language | [optional] 
 **description** | **str**| Video description | [optional] 
 **wait_transcoding** | **bool**| Whether or not we wait transcoding before publish the video | [optional] 
 **support** | **str**| A text tell the audience how to support the video creator | [optional] 
 **nsfw** | **bool**| Whether or not this video contains sensitive content | [optional] 
 **tags** | [**list[str]**](str.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
 **comments_enabled** | **bool**| Enable or disable comments for this video | [optional] 
 **download_enabled** | **bool**| Enable or disable downloading for this video | [optional] 
 **schedule_update** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional] 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**409** | HTTP or Torrent/magnetURI import not enabled |  -  |
**400** | &#x60;magnetUri&#x60; or &#x60;targetUrl&#x60; or a torrent file missing |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_languages_get**
> list[str] videos_languages_get()

List available video languages

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    
    try:
        # List available video languages
        api_response = api_instance.videos_languages_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_languages_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**list[str]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_licences_get**
> list[str] videos_licences_get()

List available video licences

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    
    try:
        # List available video licences
        api_response = api_instance.videos_licences_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_licences_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**list[str]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_live_id_get**
> LiveVideoResponse videos_live_id_get(id)

Get a live information

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid

    try:
        # Get a live information
        api_response = api_instance.videos_live_id_get(id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_live_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

[**LiveVideoResponse**](LiveVideoResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_live_id_put**
> videos_live_id_put(id, live_video_update=live_video_update)

Update a live information

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid
live_video_update = peertube.LiveVideoUpdate() # LiveVideoUpdate |  (optional)

    try:
        # Update a live information
        api_instance.videos_live_id_put(id, live_video_update=live_video_update)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_live_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **live_video_update** | [**LiveVideoUpdate**](LiveVideoUpdate.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | Successful operation |  -  |
**400** | Bad parameters or trying to update a live that has already started |  -  |
**403** | Trying to save replay of the live but saving replay is not enabled on the instance |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_live_post**
> VideoUploadResponse videos_live_post(channel_id, name, save_replay=save_replay, thumbnailfile=thumbnailfile, previewfile=previewfile, privacy=privacy, category=category, licence=licence, language=language, description=description, support=support, nsfw=nsfw, tags=tags, comments_enabled=comments_enabled, download_enabled=download_enabled)

Create a live

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    channel_id = 56 # int | Channel id that will contain this live video
name = 'name_example' # str | Live video/replay name
save_replay = True # bool |  (optional)
thumbnailfile = '/path/to/file' # file | Live video/replay thumbnail file (optional)
previewfile = '/path/to/file' # file | Live video/replay preview file (optional)
privacy = peertube.VideoPrivacySet() # VideoPrivacySet |  (optional)
category = 'category_example' # str | Live video/replay category (optional)
licence = 'licence_example' # str | Live video/replay licence (optional)
language = 'language_example' # str | Live video/replay language (optional)
description = 'description_example' # str | Live video/replay description (optional)
support = 'support_example' # str | A text tell the audience how to support the creator (optional)
nsfw = True # bool | Whether or not this live video/replay contains sensitive content (optional)
tags = 'tags_example' # list[str] | Live video/replay tags (maximum 5 tags each between 2 and 30 characters) (optional)
comments_enabled = True # bool | Enable or disable comments for this live video/replay (optional)
download_enabled = True # bool | Enable or disable downloading for the replay of this live (optional)

    try:
        # Create a live
        api_response = api_instance.videos_live_post(channel_id, name, save_replay=save_replay, thumbnailfile=thumbnailfile, previewfile=previewfile, privacy=privacy, category=category, licence=licence, language=language, description=description, support=support, nsfw=nsfw, tags=tags, comments_enabled=comments_enabled, download_enabled=download_enabled)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_live_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **channel_id** | **int**| Channel id that will contain this live video | 
 **name** | **str**| Live video/replay name | 
 **save_replay** | **bool**|  | [optional] 
 **thumbnailfile** | **file**| Live video/replay thumbnail file | [optional] 
 **previewfile** | **file**| Live video/replay preview file | [optional] 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] 
 **category** | **str**| Live video/replay category | [optional] 
 **licence** | **str**| Live video/replay licence | [optional] 
 **language** | **str**| Live video/replay language | [optional] 
 **description** | **str**| Live video/replay description | [optional] 
 **support** | **str**| A text tell the audience how to support the creator | [optional] 
 **nsfw** | **bool**| Whether or not this live video/replay contains sensitive content | [optional] 
 **tags** | [**list[str]**](str.md)| Live video/replay tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
 **comments_enabled** | **bool**| Enable or disable comments for this live video/replay | [optional] 
 **download_enabled** | **bool**| Enable or disable downloading for the replay of this live | [optional] 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**403** | Live is not enabled, allow replay is not enabled, or max instance/user live videos limit is exceeded |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_privacies_get**
> list[str] videos_privacies_get()

List available video privacies

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    
    try:
        # List available video privacies
        api_response = api_instance.videos_privacies_get()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_privacies_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**list[str]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_upload_post**
> VideoUploadResponse videos_upload_post(videofile, channel_id, name, thumbnailfile=thumbnailfile, previewfile=previewfile, privacy=privacy, category=category, licence=licence, language=language, description=description, wait_transcoding=wait_transcoding, support=support, nsfw=nsfw, tags=tags, comments_enabled=comments_enabled, download_enabled=download_enabled, originally_published_at=originally_published_at, schedule_update=schedule_update)

Upload a video

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoApi(api_client)
    videofile = '/path/to/file' # file | Video file
channel_id = 56 # int | Channel id that will contain this video
name = 'name_example' # str | Video name
thumbnailfile = '/path/to/file' # file | Video thumbnail file (optional)
previewfile = '/path/to/file' # file | Video preview file (optional)
privacy = peertube.VideoPrivacySet() # VideoPrivacySet |  (optional)
category = 56 # int | Video category (optional)
licence = 'licence_example' # str | Video licence (optional)
language = 56 # int | Video language (optional)
description = 'description_example' # str | Video description (optional)
wait_transcoding = True # bool | Whether or not we wait transcoding before publish the video (optional)
support = 'support_example' # str | A text tell the audience how to support the video creator (optional)
nsfw = True # bool | Whether or not this video contains sensitive content (optional)
tags = 'tags_example' # list[str] | Video tags (maximum 5 tags each between 2 and 30 characters) (optional)
comments_enabled = True # bool | Enable or disable comments for this video (optional)
download_enabled = True # bool | Enable or disable downloading for this video (optional)
originally_published_at = '2013-10-20T19:20:30+01:00' # datetime | Date when the content was originally published (optional)
schedule_update = peertube.VideoScheduledUpdate() # VideoScheduledUpdate |  (optional)

    try:
        # Upload a video
        api_response = api_instance.videos_upload_post(videofile, channel_id, name, thumbnailfile=thumbnailfile, previewfile=previewfile, privacy=privacy, category=category, licence=licence, language=language, description=description, wait_transcoding=wait_transcoding, support=support, nsfw=nsfw, tags=tags, comments_enabled=comments_enabled, download_enabled=download_enabled, originally_published_at=originally_published_at, schedule_update=schedule_update)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoApi->videos_upload_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **videofile** | **file**| Video file | 
 **channel_id** | **int**| Channel id that will contain this video | 
 **name** | **str**| Video name | 
 **thumbnailfile** | **file**| Video thumbnail file | [optional] 
 **previewfile** | **file**| Video preview file | [optional] 
 **privacy** | [**VideoPrivacySet**](VideoPrivacySet.md)|  | [optional] 
 **category** | **int**| Video category | [optional] 
 **licence** | **str**| Video licence | [optional] 
 **language** | **int**| Video language | [optional] 
 **description** | **str**| Video description | [optional] 
 **wait_transcoding** | **bool**| Whether or not we wait transcoding before publish the video | [optional] 
 **support** | **str**| A text tell the audience how to support the video creator | [optional] 
 **nsfw** | **bool**| Whether or not this video contains sensitive content | [optional] 
 **tags** | [**list[str]**](str.md)| Video tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
 **comments_enabled** | **bool**| Enable or disable comments for this video | [optional] 
 **download_enabled** | **bool**| Enable or disable downloading for this video | [optional] 
 **originally_published_at** | **datetime**| Date when the content was originally published | [optional] 
 **schedule_update** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md)|  | [optional] 

### Return type

[**VideoUploadResponse**](VideoUploadResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**403** | user video quota is exceeded with this video |  -  |
**408** | upload has timed out |  -  |
**422** | invalid input file |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

