# ServerConfigUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**video_quota** | **int** |  | [optional] 
**video_quota_daily** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


