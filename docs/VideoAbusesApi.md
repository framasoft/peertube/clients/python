# peertube.VideoAbusesApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videos_abuse_get**](VideoAbusesApi.md#videos_abuse_get) | **GET** /videos/abuse | List video abuses
[**videos_id_abuse_abuse_id_delete**](VideoAbusesApi.md#videos_id_abuse_abuse_id_delete) | **DELETE** /videos/{id}/abuse/{abuseId} | Delete an abuse
[**videos_id_abuse_abuse_id_put**](VideoAbusesApi.md#videos_id_abuse_abuse_id_put) | **PUT** /videos/{id}/abuse/{abuseId} | Update an abuse
[**videos_id_abuse_post**](VideoAbusesApi.md#videos_id_abuse_post) | **POST** /videos/{id}/abuse | Report an abuse


# **videos_abuse_get**
> list[VideoAbuse] videos_abuse_get(id=id, predefined_reason=predefined_reason, search=search, state=state, search_reporter=search_reporter, search_reportee=search_reportee, search_video=search_video, search_video_channel=search_video_channel, start=start, count=count, sort=sort)

List video abuses

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoAbusesApi(api_client)
    id = 56 # int | only list the report with this id (optional)
predefined_reason = 'predefined_reason_example' # str | predefined reason the listed reports should contain (optional)
search = 'search_example' # str | plain search that will match with video titles, reporter names and more (optional)
state = 56 # int | The video playlist privacy (Pending = `1`, Rejected = `2`, Accepted = `3`) (optional)
search_reporter = 'search_reporter_example' # str | only list reports of a specific reporter (optional)
search_reportee = 'search_reportee_example' # str | only list reports of a specific reportee (optional)
search_video = 'search_video_example' # str | only list reports of a specific video (optional)
search_video_channel = 'search_video_channel_example' # str | only list reports of a specific video channel (optional)
start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = 'sort_example' # str | Sort abuses by criteria (optional)

    try:
        # List video abuses
        api_response = api_instance.videos_abuse_get(id=id, predefined_reason=predefined_reason, search=search, state=state, search_reporter=search_reporter, search_reportee=search_reportee, search_video=search_video, search_video_channel=search_video_channel, start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling VideoAbusesApi->videos_abuse_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| only list the report with this id | [optional] 
 **predefined_reason** | **str**| predefined reason the listed reports should contain | [optional] 
 **search** | **str**| plain search that will match with video titles, reporter names and more | [optional] 
 **state** | **int**| The video playlist privacy (Pending &#x3D; &#x60;1&#x60;, Rejected &#x3D; &#x60;2&#x60;, Accepted &#x3D; &#x60;3&#x60;) | [optional] 
 **search_reporter** | **str**| only list reports of a specific reporter | [optional] 
 **search_reportee** | **str**| only list reports of a specific reportee | [optional] 
 **search_video** | **str**| only list reports of a specific video | [optional] 
 **search_video_channel** | **str**| only list reports of a specific video channel | [optional] 
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort abuses by criteria | [optional] 

### Return type

[**list[VideoAbuse]**](VideoAbuse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_abuse_abuse_id_delete**
> videos_id_abuse_abuse_id_delete(id, abuse_id)

Delete an abuse

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoAbusesApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid
abuse_id = 56 # int | Video abuse id

    try:
        # Delete an abuse
        api_instance.videos_id_abuse_abuse_id_delete(id, abuse_id)
    except ApiException as e:
        print("Exception when calling VideoAbusesApi->videos_id_abuse_abuse_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **abuse_id** | **int**| Video abuse id | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**404** | block not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_abuse_abuse_id_put**
> videos_id_abuse_abuse_id_put(id, abuse_id, inline_object10=inline_object10)

Update an abuse

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoAbusesApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid
abuse_id = 56 # int | Video abuse id
inline_object10 = peertube.InlineObject10() # InlineObject10 |  (optional)

    try:
        # Update an abuse
        api_instance.videos_id_abuse_abuse_id_put(id, abuse_id, inline_object10=inline_object10)
    except ApiException as e:
        print("Exception when calling VideoAbusesApi->videos_id_abuse_abuse_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **abuse_id** | **int**| Video abuse id | 
 **inline_object10** | [**InlineObject10**](InlineObject10.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**404** | video abuse not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_id_abuse_post**
> videos_id_abuse_post(id, inline_object9)

Report an abuse

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoAbusesApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid
inline_object9 = peertube.InlineObject9() # InlineObject9 | 

    try:
        # Report an abuse
        api_instance.videos_id_abuse_post(id, inline_object9)
    except ApiException as e:
        print("Exception when calling VideoAbusesApi->videos_id_abuse_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **inline_object9** | [**InlineObject9**](InlineObject9.md)|  | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**400** | incorrect request parameters |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

