# InlineObject10

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reason** | **str** | Reason why the user reports this video | 
**predefined_reasons** | **list[str]** | Reason categories that help triage reports | [optional] 
**video** | [**AbusesVideo**](AbusesVideo.md) |  | [optional] 
**comment** | [**AbusesComment**](AbusesComment.md) |  | [optional] 
**account** | [**AbusesAccount**](AbusesAccount.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


