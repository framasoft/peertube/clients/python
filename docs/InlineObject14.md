# InlineObject14

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | **str** | Video playlist display name | 
**thumbnailfile** | **file** | Video playlist thumbnail file | [optional] 
**privacy** | [**VideoPlaylistPrivacySet**](VideoPlaylistPrivacySet.md) |  | [optional] 
**description** | **str** | Video playlist description | [optional] 
**video_channel_id** | **int** | Video channel in which the playlist will be published | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


