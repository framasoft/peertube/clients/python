# peertube.PluginsApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**plugins_available_get**](PluginsApi.md#plugins_available_get) | **GET** /plugins/available | List available plugins
[**plugins_get**](PluginsApi.md#plugins_get) | **GET** /plugins | List plugins
[**plugins_install_post**](PluginsApi.md#plugins_install_post) | **POST** /plugins/install | Install a plugin
[**plugins_npm_name_get**](PluginsApi.md#plugins_npm_name_get) | **GET** /plugins/{npmName} | Get a plugin
[**plugins_npm_name_public_settings_get**](PluginsApi.md#plugins_npm_name_public_settings_get) | **GET** /plugins/{npmName}/public-settings | Get a plugin&#39;s public settings
[**plugins_npm_name_registered_settings_get**](PluginsApi.md#plugins_npm_name_registered_settings_get) | **GET** /plugins/{npmName}/registered-settings | Get a plugin&#39;s registered settings
[**plugins_npm_name_settings_put**](PluginsApi.md#plugins_npm_name_settings_put) | **PUT** /plugins/{npmName}/settings | Set a plugin&#39;s settings
[**plugins_uninstall_post**](PluginsApi.md#plugins_uninstall_post) | **POST** /plugins/uninstall | Uninstall a plugin
[**plugins_update_post**](PluginsApi.md#plugins_update_post) | **POST** /plugins/update | Update a plugin


# **plugins_available_get**
> PluginResponse plugins_available_get(search=search, plugin_type=plugin_type, current_peer_tube_engine=current_peer_tube_engine, start=start, count=count, sort=sort)

List available plugins

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.PluginsApi(api_client)
    search = 'search_example' # str |  (optional)
plugin_type = 56 # int |  (optional)
current_peer_tube_engine = 'current_peer_tube_engine_example' # str |  (optional)
start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = '-createdAt' # str | Sort column (optional)

    try:
        # List available plugins
        api_response = api_instance.plugins_available_get(search=search, plugin_type=plugin_type, current_peer_tube_engine=current_peer_tube_engine, start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PluginsApi->plugins_available_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search** | **str**|  | [optional] 
 **plugin_type** | **int**|  | [optional] 
 **current_peer_tube_engine** | **str**|  | [optional] 
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort column | [optional] 

### Return type

[**PluginResponse**](PluginResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**503** | plugin index unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plugins_get**
> PluginResponse plugins_get(plugin_type=plugin_type, uninstalled=uninstalled, start=start, count=count, sort=sort)

List plugins

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.PluginsApi(api_client)
    plugin_type = 56 # int |  (optional)
uninstalled = True # bool |  (optional)
start = 56 # int | Offset used to paginate results (optional)
count = 15 # int | Number of items to return (optional) (default to 15)
sort = '-createdAt' # str | Sort column (optional)

    try:
        # List plugins
        api_response = api_instance.plugins_get(plugin_type=plugin_type, uninstalled=uninstalled, start=start, count=count, sort=sort)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PluginsApi->plugins_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plugin_type** | **int**|  | [optional] 
 **uninstalled** | **bool**|  | [optional] 
 **start** | **int**| Offset used to paginate results | [optional] 
 **count** | **int**| Number of items to return | [optional] [default to 15]
 **sort** | **str**| Sort column | [optional] 

### Return type

[**PluginResponse**](PluginResponse.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plugins_install_post**
> plugins_install_post(unknown_base_type=unknown_base_type)

Install a plugin

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.PluginsApi(api_client)
    unknown_base_type = peertube.UNKNOWN_BASE_TYPE() # UNKNOWN_BASE_TYPE |  (optional)

    try:
        # Install a plugin
        api_instance.plugins_install_post(unknown_base_type=unknown_base_type)
    except ApiException as e:
        print("Exception when calling PluginsApi->plugins_install_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unknown_base_type** | [**UNKNOWN_BASE_TYPE**](UNKNOWN_BASE_TYPE.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**400** | should have either &#x60;npmName&#x60; or &#x60;path&#x60; set |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plugins_npm_name_get**
> Plugin plugins_npm_name_get(npm_name)

Get a plugin

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.PluginsApi(api_client)
    npm_name = 'peertube-plugin-auth-ldap' # str | name of the plugin/theme on npmjs.com or in its package.json

    try:
        # Get a plugin
        api_response = api_instance.plugins_npm_name_get(npm_name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PluginsApi->plugins_npm_name_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **npm_name** | **str**| name of the plugin/theme on npmjs.com or in its package.json | 

### Return type

[**Plugin**](Plugin.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | plugin not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plugins_npm_name_public_settings_get**
> dict(str, object) plugins_npm_name_public_settings_get(npm_name)

Get a plugin's public settings

### Example

```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)


# Enter a context with an instance of the API client
with peertube.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = peertube.PluginsApi(api_client)
    npm_name = 'peertube-plugin-auth-ldap' # str | name of the plugin/theme on npmjs.com or in its package.json

    try:
        # Get a plugin's public settings
        api_response = api_instance.plugins_npm_name_public_settings_get(npm_name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PluginsApi->plugins_npm_name_public_settings_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **npm_name** | **str**| name of the plugin/theme on npmjs.com or in its package.json | 

### Return type

**dict(str, object)**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | plugin not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plugins_npm_name_registered_settings_get**
> dict(str, object) plugins_npm_name_registered_settings_get(npm_name)

Get a plugin's registered settings

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.PluginsApi(api_client)
    npm_name = 'peertube-plugin-auth-ldap' # str | name of the plugin/theme on npmjs.com or in its package.json

    try:
        # Get a plugin's registered settings
        api_response = api_instance.plugins_npm_name_registered_settings_get(npm_name)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling PluginsApi->plugins_npm_name_registered_settings_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **npm_name** | **str**| name of the plugin/theme on npmjs.com or in its package.json | 

### Return type

**dict(str, object)**

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | plugin not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plugins_npm_name_settings_put**
> plugins_npm_name_settings_put(npm_name, inline_object26=inline_object26)

Set a plugin's settings

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.PluginsApi(api_client)
    npm_name = 'peertube-plugin-auth-ldap' # str | name of the plugin/theme on npmjs.com or in its package.json
inline_object26 = peertube.InlineObject26() # InlineObject26 |  (optional)

    try:
        # Set a plugin's settings
        api_instance.plugins_npm_name_settings_put(npm_name, inline_object26=inline_object26)
    except ApiException as e:
        print("Exception when calling PluginsApi->plugins_npm_name_settings_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **npm_name** | **str**| name of the plugin/theme on npmjs.com or in its package.json | 
 **inline_object26** | [**InlineObject26**](InlineObject26.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**404** | plugin not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plugins_uninstall_post**
> plugins_uninstall_post(inline_object25=inline_object25)

Uninstall a plugin

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.PluginsApi(api_client)
    inline_object25 = peertube.InlineObject25() # InlineObject25 |  (optional)

    try:
        # Uninstall a plugin
        api_instance.plugins_uninstall_post(inline_object25=inline_object25)
    except ApiException as e:
        print("Exception when calling PluginsApi->plugins_uninstall_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inline_object25** | [**InlineObject25**](InlineObject25.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**404** | existing plugin not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plugins_update_post**
> plugins_update_post(unknown_base_type=unknown_base_type)

Update a plugin

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.PluginsApi(api_client)
    unknown_base_type = peertube.UNKNOWN_BASE_TYPE() # UNKNOWN_BASE_TYPE |  (optional)

    try:
        # Update a plugin
        api_instance.plugins_update_post(unknown_base_type=unknown_base_type)
    except ApiException as e:
        print("Exception when calling PluginsApi->plugins_update_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unknown_base_type** | [**UNKNOWN_BASE_TYPE**](UNKNOWN_BASE_TYPE.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**400** | should have either &#x60;npmName&#x60; or &#x60;path&#x60; set |  -  |
**404** | existing plugin not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

