# peertube.VideoOwnershipChangeApi

All URIs are relative to *https://peertube2.cpy.re/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**videos_id_give_ownership_post**](VideoOwnershipChangeApi.md#videos_id_give_ownership_post) | **POST** /videos/{id}/give-ownership | Request ownership change
[**videos_ownership_get**](VideoOwnershipChangeApi.md#videos_ownership_get) | **GET** /videos/ownership | List video ownership changes
[**videos_ownership_id_accept_post**](VideoOwnershipChangeApi.md#videos_ownership_id_accept_post) | **POST** /videos/ownership/{id}/accept | Accept ownership change request
[**videos_ownership_id_refuse_post**](VideoOwnershipChangeApi.md#videos_ownership_id_refuse_post) | **POST** /videos/ownership/{id}/refuse | Refuse ownership change request


# **videos_id_give_ownership_post**
> videos_id_give_ownership_post(id, username)

Request ownership change

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoOwnershipChangeApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid
username = 'username_example' # str | 

    try:
        # Request ownership change
        api_instance.videos_id_give_ownership_post(id, username)
    except ApiException as e:
        print("Exception when calling VideoOwnershipChangeApi->videos_id_give_ownership_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 
 **username** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**400** | changing video ownership to a remote account is not supported yet |  -  |
**404** | video not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_ownership_get**
> videos_ownership_get()

List video ownership changes

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoOwnershipChangeApi(api_client)
    
    try:
        # List video ownership changes
        api_instance.videos_ownership_get()
    except ApiException as e:
        print("Exception when calling VideoOwnershipChangeApi->videos_ownership_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_ownership_id_accept_post**
> videos_ownership_id_accept_post(id)

Accept ownership change request

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoOwnershipChangeApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid

    try:
        # Accept ownership change request
        api_instance.videos_ownership_id_accept_post(id)
    except ApiException as e:
        print("Exception when calling VideoOwnershipChangeApi->videos_ownership_id_accept_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**403** | cannot terminate an ownership change of another user |  -  |
**404** | video owneship change not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **videos_ownership_id_refuse_post**
> videos_ownership_id_refuse_post(id)

Refuse ownership change request

### Example

* OAuth Authentication (OAuth2):
```python
from __future__ import print_function
import time
import peertube
from peertube.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://peertube2.cpy.re/api/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: OAuth2
configuration = peertube.Configuration(
    host = "https://peertube2.cpy.re/api/v1"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with peertube.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = peertube.VideoOwnershipChangeApi(api_client)
    id = peertube.OneOfintegerUUID() # OneOfintegerUUID | The object id or uuid

    try:
        # Refuse ownership change request
        api_instance.videos_ownership_id_refuse_post(id)
    except ApiException as e:
        print("Exception when calling VideoOwnershipChangeApi->videos_ownership_id_refuse_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**OneOfintegerUUID**](.md)| The object id or uuid | 

### Return type

void (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**403** | cannot terminate an ownership change of another user |  -  |
**404** | video owneship change not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

