# InlineObject6

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**thumbnailfile** | **file** | Video thumbnail file | [optional] 
**previewfile** | **file** | Video preview file | [optional] 
**category** | **int** | Video category | [optional] 
**licence** | **int** | Video licence | [optional] 
**language** | **str** | Video language | [optional] 
**privacy** | [**VideoPrivacySet**](VideoPrivacySet.md) |  | [optional] 
**description** | **str** | Video description | [optional] 
**wait_transcoding** | **str** | Whether or not we wait transcoding before publish the video | [optional] 
**support** | **str** | A text tell the audience how to support the video creator | [optional] 
**nsfw** | **bool** | Whether or not this video contains sensitive content | [optional] 
**name** | **str** | Video name | [optional] 
**tags** | **list[str]** | Video tags (maximum 5 tags each between 2 and 30 characters) | [optional] 
**comments_enabled** | **bool** | Enable or disable comments for this video | [optional] 
**originally_published_at** | **datetime** | Date when the content was originally published | [optional] 
**schedule_update** | [**VideoScheduledUpdate**](VideoScheduledUpdate.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


