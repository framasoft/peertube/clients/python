# coding: utf-8

"""
    PeerTube

    # Introduction  The PeerTube API is built on HTTP(S). Our API is RESTful. It has predictable resource URLs. It returns HTTP response codes to indicate errors. It also accepts and returns JSON in the HTTP body. You can use your favorite HTTP/REST library for your programming language to use PeerTube. No official SDK is currently provided, but the spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice.  # Authentication  When you sign up for an account, you are given the possibility to generate sessions, and authenticate using this session token. One session token can currently be used at a time.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call. The body of the response will be JSON in the following format.  ``` {   \"code\": \"unauthorized_request\", // example inner error code   \"error\": \"Token is invalid.\" // example exposed error message } ```   # noqa: E501

    The version of the OpenAPI document: 2.1.0
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import peertube
from peertube.api.user_api import UserApi  # noqa: E501
from peertube.rest import ApiException


class TestUserApi(unittest.TestCase):
    """UserApi unit test stubs"""

    def setUp(self):
        self.api = peertube.api.user_api.UserApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_accounts_name_ratings_get(self):
        """Test case for accounts_name_ratings_get

        Get ratings of an account by its name  # noqa: E501
        """
        pass

    def test_users_get(self):
        """Test case for users_get

        Get a list of users  # noqa: E501
        """
        pass

    def test_users_id_delete(self):
        """Test case for users_id_delete

        Delete a user by its id  # noqa: E501
        """
        pass

    def test_users_id_get(self):
        """Test case for users_id_get

        Get user by its id  # noqa: E501
        """
        pass

    def test_users_id_put(self):
        """Test case for users_id_put

        Update user profile by its id  # noqa: E501
        """
        pass

    def test_users_post(self):
        """Test case for users_post

        Creates user  # noqa: E501
        """
        pass

    def test_users_register_post(self):
        """Test case for users_register_post

        Register a user  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
