# coding: utf-8

"""
    PeerTube

    # Introduction  The PeerTube API is built on HTTP(S) and is RESTful. You can use your favorite HTTP/REST library for your programming language to use PeerTube. The spec API is fully compatible with [openapi-generator](https://github.com/OpenAPITools/openapi-generator/wiki/API-client-generator-HOWTO) which generates a client SDK in the language of your choice - we generate some client SDKs automatically:  - [Python](https://framagit.org/framasoft/peertube/clients/python) - [Go](https://framagit.org/framasoft/peertube/clients/go) - [Kotlin](https://framagit.org/framasoft/peertube/clients/kotlin)  See the [Quick Start guide](https://docs.joinpeertube.org/#/api-rest-getting-started) so you can play with the PeerTube API.  # Authentication  When you sign up for an account, you are given the possibility to generate sessions, and authenticate using this session token. One session token can currently be used at a time.  # Errors  The API uses standard HTTP status codes to indicate the success or failure of the API call. The body of the response will be JSON in the following format.  ``` {   \"code\": \"unauthorized_request\", // example inner error code   \"error\": \"Token is invalid.\" // example exposed error message } ```   # noqa: E501

    The version of the OpenAPI document: 2.2.0
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest

import peertube
from peertube.api.plugins_api import PluginsApi  # noqa: E501
from peertube.rest import ApiException


class TestPluginsApi(unittest.TestCase):
    """PluginsApi unit test stubs"""

    def setUp(self):
        self.api = peertube.api.plugins_api.PluginsApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_plugins_available_get(self):
        """Test case for plugins_available_get

        List available plugins  # noqa: E501
        """
        pass

    def test_plugins_get(self):
        """Test case for plugins_get

        List plugins  # noqa: E501
        """
        pass

    def test_plugins_install_post(self):
        """Test case for plugins_install_post

        Install a plugin  # noqa: E501
        """
        pass

    def test_plugins_npm_name_get(self):
        """Test case for plugins_npm_name_get

        Get a plugin  # noqa: E501
        """
        pass

    def test_plugins_npm_name_public_settings_get(self):
        """Test case for plugins_npm_name_public_settings_get

        Get a plugin's public settings  # noqa: E501
        """
        pass

    def test_plugins_npm_name_registered_settings_get(self):
        """Test case for plugins_npm_name_registered_settings_get

        Get a plugin's registered settings  # noqa: E501
        """
        pass

    def test_plugins_npm_name_settings_put(self):
        """Test case for plugins_npm_name_settings_put

        Set a plugin's settings  # noqa: E501
        """
        pass

    def test_plugins_uninstall_post(self):
        """Test case for plugins_uninstall_post

        Uninstall a plugin  # noqa: E501
        """
        pass

    def test_plugins_update_post(self):
        """Test case for plugins_update_post

        Update a plugin  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
